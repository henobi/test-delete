module.exports = function (Config) {        // eslint-disable-line id-match
    "use strict";

    /* eslint-disable no-unused-vars*/
    require("./karma.conf")(Config);
    /* eslint-enable no-unused-vars*/

    Config.set({

        browsers: ["ChromeHeadless"],

        singleRun: true

    });
};
