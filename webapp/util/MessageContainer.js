sap.ui.define([
    "sap/ui/base/Object",
    "sap/m/MessageToast",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/m/Text",
    "sap/m/MessageBox"

], function (Object, MessageToast, Button, Dialog, Text, MessageBox) {        // eslint-disable-line id-match
    "use strict";

    var oInstance;

    /**
     * MessageContainer.js
     *
     * This object is a message container, which receives messages with a priority and saves them until they are needed.
     * When the messages are shown, the stack will be cleared.
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {Object} [mSettings] initial settings for the new control
     *
     * @class MessageContainer.js
     *
     * @extends porsche.cats.timerecording.util.MessageContainer
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.MessageContainer
     */
    var MessageContainer = Object.extend("porsche.cats.timerecording.util.MessageContainer", {    // eslint-disable-line id-match

        /**
         * Constructor function
         * @memberOf porsche.cats.timerecording.MessageContainer
         */
        constructor: function () {
            this.aMessages = [];
        },

        /**
         *
         * @param {Array} [aArray] The array with the included messages
         * @param {string} [sKey] The key of the array which shall be compared
         * @returns {Object} The message object with the highest key
         * @memberOf porsche.cats.timerecording.MessageContainer
         * @private
         */
        _getHighest: function (aArray, sKey) {
            return aArray.reduce(function (sPrev, sCurr) {
                return sPrev[sKey] > sCurr[sKey] ? sPrev : sCurr;
            });
        },

        /**
         * Adds a message to the stack
         * @param {string} [sText] The message
         * @param {number} [iPriority] The priority of the message
         * @param {string} [sState] The state of the message
         * @memberOf porsche.cats.timerecording.MessageContainer
         */
        addMessage: function (sText, iPriority, sState) {
            var oMsg = {
                text: sText,
                priority: iPriority,
                state: sState
            };
            this.aMessages.push(oMsg);
        },

        /**
         * Shows the message with the highest priority and clears the stack
         * @memberOf porsche.cats.timerecording.MessageContainer
         */
        showHighestMessage: function () {

            if (this.aMessages.length > 0) {
                // get the highest message from the message array based on priority
                var oMsg = this._getHighest(this.aMessages, "priority");

                // show the highest message
                if (oMsg.state === "Error") {

                    MessageBox.error(oMsg.text);

                } else {
                    MessageToast.show(oMsg.text);
                }

                // clear the message array afterwards
                this.aMessages = [];
            }
        }
    });
    return {

        /**
         * Returns an instance of the message container
         * @returns {porsche.cats.timerecording.util.MessageContainer} The message container object
         * @memberOf porsche.cats.timerecording.MessageContainer
         */
        getInstance: function () {
            if (!oInstance) {
                oInstance = new MessageContainer();
            }
            return oInstance;
        }
    };
});
