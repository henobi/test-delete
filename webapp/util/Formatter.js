sap.ui.define([], function () {       // eslint-disable-line id-match
    "use strict";

    return {

        /**
         * Converts a string to a float value
         * @param {string} [sValue] The string decimal value
         * @returns {float} The floating number converted value
         * @memberOf porsche.cats.timerecording.Formatter
         */
        parseFloat: function (sValue) {
            return parseFloat(sValue);
        },

        /**
         *  Parses a value to integer
         *  @param {string} sValue - the value to be parsed
         *  @returns {int} returns formatted value
         *  @memberOf porsche.cats.timerecording.Formatter
         */
        integerParseInt: function (sValue) {
            var sInt = 0;
            if (sValue) {
                sInt = parseInt(sValue, 10);
            }
            return sInt;
        },

        /**
         * Converts a float to percentage value
         * @param {float} [sValue] The float percentage value
         * @returns {integer} The percentage number converted value
         * @memberOf porsche.cats.timerecording.Formatter
         */
        percentage: function (sValue) {
            var iValue = (sValue * 100);
            iValue = parseInt(iValue, 10);
            return iValue;
        },

        /**
         * Rounds the number unit value to 2 digits
         * @param {string} [sValue] The number string to be rounded
         * @returns {string} sValue with 2 digits rounded
         * @memberOf porsche.cats.timerecording.Formatter
         */
        numberFixedToTwo: function (sValue) {
            if (!sValue) {
                return "";
            }

            return parseFloat(sValue).toFixed(2);
        },

        /**
         * Formats a date as UTC date
         * @param {Date} [oDate] Date in local format
         * @returns {Date} The date in UTC format
         * @memberOf porsche.cats.timerecording.Formatter
         */
        formatAsUtcDate: function (oDate) {
            return new Date(Date.UTC(
                oDate.getFullYear(),
                oDate.getMonth(),
                oDate.getDate()
            ));
        },

        /**
         * Formats a date as UTC date with 0 o'clock
         * @param {Date} [oDate] Date in local format
         * @returns {Date} The date in UTC format
         * @memberOf porsche.cats.timerecording.Formatter
         */
        formatAsZeroHourUtcDate: function (oDate) {

            var oFormattedDate = new Date(Date.UTC(
                oDate.getFullYear(),
                oDate.getMonth(),
                oDate.getDate()
            ));
            // Remove any remaining hours
            oFormattedDate.setHours(0);

            return oFormattedDate;
        },

        /**
         * Formats the day status as calendar type
         * @param {string} [sStatus] The status of the current day as character key
         * @returns {sap.ui.unified.CalendarDayType} Types of a calendar day used for visualization.
         * @memberOf porsche.cats.timerecording.Formatter
         */
        addSpecialDate: function (sStatus) {

            var sType;

            // Format the status from backend as sap.ui.unified.CalendarDayType
            switch (sStatus) {
                case "A":
                    sType = "Type01";
                    break;
                case "C":
                    sType = "Type02";
                    break;
                case "P":
                case "N":
                case "O":
                case "M":
                    sType = "Type03";
                    break;
                case "T":
                    sType = "Type04";
                    break;
                case "U":
                default:
                    sType = "None";
                    break;
            }

            return sType;
        },

        /**
         * Formats the value as technical readable value
         * @param {sap.ui.model.type} [oType] This class represents simple types.
         * @param {string} [sValue] The original value with language dependent separator
         * @returns {string} The decimal value formatted as string with "." as separator
         * @memberOf porsche.cats.timerecording.Formatter
         */
        formatDecimalAsFloatString: function (oType, sValue) {

            var sSeparator;

            // Try to find decimal separator depending on type (oData or edm)
            if (oType.oOutputFormat && oType.oOutputFormat.oFormatOptions && oType.oOutputFormat.oFormatOptions.decimalSeparator) {
                sSeparator = oType.oOutputFormat.oFormatOptions.decimalSeparator;
            } else if (oType.oFormat && oType.oFormat.oFormatOptions && oType.oFormat.oFormatOptions.decimalSeparator) {
                sSeparator = oType.oFormat.oFormatOptions.decimalSeparator;
            }

            if (sSeparator && sSeparator !== ".") {
                return sValue.toString().replace(/\./g, "").replace(sSeparator, ".");
            } else {
                return sValue;
            }
        },

        /**
         * Returns the correct target hour value, depending on the current selection method
         * @param {string} [sTargetHours] The target hour value for interval selection
         * @param {string} [sTargetHoursSingle] The target hour value for single selection
         * @param {string} [sSelectionMethod] The current selection method
         * @returns {string} The correct value for the current selection method
         * @memberOf porsche.cats.timerecording.Formatter
         */
        getTargetValueDependingOnSelectionMethod: function (sTargetHours, sTargetHoursSingle, sSelectionMethod) {
            return (sSelectionMethod === "single") ? sTargetHoursSingle : sTargetHours;
        },

        /**
         * Returns a concatenated string of ID and description for the loan cost centers
         * @param {string} [oId] The ID of the loan cost center
         * @param {string} [oDesc] The description of the loan cost center (can be empty)
         * @returns {string} The concatenated string (empty if ID is not provided)
         * @memberOf porsche.cats.timerecording.Formatter
         */
        formatLoanCostCenterForSelect: function (oId, oDesc) {
            if (oDesc) {
                return oId + " - " + oDesc;
            } else if (oId) {
                return oId;
            } else {
                return " ";
            }
        },

        /**
         * Returns error state for the difference if it's not zero
         * @param {float} [sDifference] The difference value
         * @returns {sap.ui.core.ValueState} The state for object number
         * @memberOf porsche.cats.timerecording.Formatter
         */
        formatDifferenceState: function (sDifference) {
            if (!sDifference) {
                return sap.ui.core.ValueState.None;
            } else if (parseFloat(sDifference) === 0) {
                return sap.ui.core.ValueState.Success;
            } else {
                return sap.ui.core.ValueState.Error;
            }
        }

    };

});
