sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device"

], function (JSONModel, Device) {       // eslint-disable-line id-match
    "use strict";

    return {

        /**
         * Creates and returns a device model in JSON format
         * @returns {sap.ui.model.json.JSONModel} The device model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createDeviceModel: function () {
            var oDeviceModel = new JSONModel(Device);
            oDeviceModel.setDefaultBindingMode("OneWay");
            return oDeviceModel;
        },

        /**
         * Creates and returns a layout model in JSON format
         * @returns {sap.ui.model.json.JSONModel} The layout model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createLayoutModel: function () {
            return new JSONModel({
                busy: true,
                delay: 0,
                layout: "TwoColumnsMidExpanded",
                smallScreenMode: true,
                forcedLayout: false
            });
        },

        /**
         * Creates and returns a view model in JSON format
         * @returns {sap.ui.model.json.JSONModel} The layout model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createViewModel: function () {
            return new JSONModel({
                editable: false,
                enabled: false,
                selectionMode: "single",
                route: "",
                timeManagement: false,
                pernr: ""
            });
        },

        /**
         * Creates and returns a model for controlling the responsiveness of the account assignment and the user settings table
         * @returns {sap.ui.model.json.JSONModel} The layout model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createTableModel: function () {
            return new JSONModel({
                actionButtonsVisibleOnAccountAssignmentTable: true,
                actionButtonsVisibleOnFavoriteTable: true,
                actionButtonsVisibleOnLoanCostCenterTable: true,
                hoursTransferredVisible: true
            });
        },

        /**
         * Creates and returns a JSON model for a new time recording object
         * @returns {sap.ui.model.json.JSONModel} The layout model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createTimeRecordingModelForFunctionImport: function () {
            return new JSONModel({
                Pernr: "",
                CaleSelDateBegin: null,
                CaleSelDateEnd: null,
                CostObjectType: "",
                CostObjectID: "",
                AssignedHours: 0.00,
                Percentage: 0.00,
                HoursRemaining: 0.00,
                LoanCostCenter: "",
                CaseNumber: ""
            });
        },

        /**
         * Creates and returns a JSON model for the header of the account assignment page
         * @returns {sap.ui.model.json.JSONModel} The layout model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createAccountAssignmentHeaderModel: function () {
            return new JSONModel({
                StartDate: null,
                EndDate: null,
                TotalHours: 0.00,
                ShowLoanCostCenterColumn: false
            });
        },

        /**
         * Creates and returns a JSON model for the tokenizer
         * @returns {sap.ui.model.json.JSONModel} The layout model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createTokenizerModel: function () {
            return new JSONModel({
                tableId: "",
                tokens: [],
                hasTokens: false,
                applyLimit: false,
                limitReached: false,  /* Is set to true if no more slots are available */
                freeSlots: 0,      /* Number of slots that can be added by the search help */
                warningMessage: ""    /* This warning message will be displayed if no more slots are available */
            });
        },

        /**
         * Creates a model for the filters of the user table
         * @returns {sap.ui.model.json.JSONModel} The filter model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createUserFilterModel: function () {
            return new JSONModel({
                fromDate: null,
                toDate: null,
                departments: [],
                costCenters: [],
                employees: [],
                difference: null
            });
        },

        /**
         * Creates a model for the temporary employee model
         * @returns {sap.ui.model.json.JSONModel} The employee model in JSON format
         * @memberOf porsche.cats.timerecording.Models
         */
        createEmployeeModel: function () {
            return new JSONModel({
                newEmployees: [],
                selectedEmployees: []
            });
        }
    };
});
