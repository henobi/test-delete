/* eslint-disable */
sap.ui.define([
    "sap/ui/test/Opa5",
    "./arrangements/Startup"

], function (Opa5, StartUp) {
    "use strict";

    Opa5.extendConfig({
        arrangements: new StartUp(),
        viewNamespace: "porsche.cats.timerecording.view.",
        autoWait: true
    });

});
