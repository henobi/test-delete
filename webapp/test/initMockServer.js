sap.ui.define([
    "../localService/mockserver",
    "sap/m/MessageBox"

], function (MockServer, MessageBox) {      // eslint-disable-line id-match
    "use strict";

    var aMockservers = [];

    // initialize the mock server
    aMockservers.push(MockServer.init());

    Promise.all(aMockservers).catch(function (oError) {
        MessageBox.error(oError.message);
    }).finally(function () {
        // initialize the embedded component on the HTML page
        sap.ui.require(["sap/ui/core/ComponentSupport"]);
    });
});
