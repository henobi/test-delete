QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "porsche/cats/timerecording/test/unit/AllTests"
    ], function () {
        QUnit.start();
    });
});
