/**
 * Created by mdeppe on 25.10.2019.
 */
sap.ui.define([
    "sap/ui/core/UIComponent",
    "porsche/cats/timerecording/util/Models",
    "sap/ui/Device",
    "sap/m/MessageBox",
    "porsche/cats/timerecording/controller/dialog/OnBehalfDialog",
    "sap/ui/core/IconPool"

], function (UIComponent, Models, Device, MessageBox, OnBehalfDialog, IconPool) {         // eslint-disable-line id-match, max-params
    "use strict";

    // noinspection UnnecessaryLocalVariableJS
    /**
     * Component.js
     *
     * The component class provides specific metadata for components by extending the ManagedObject class.
     * The UIComponent class provides additional metadata for the configuration of user interfaces
     * or the navigation between views.
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {Object} [mSettings] initial settings for the new control
     *
     * @class Component.js
     *
     * @extends sap.ui.core.UIComponent
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.Component
     */
    return UIComponent.extend("porsche.cats.timerecording.Component", {

        metadata: {
            manifest: "json"
        },

        /**
         * Initiates the application at first load
         * @memberOf porsche.cats.timerecording.Component
         */
        init: function () {
            // call the init function of the parent
            UIComponent.prototype.init.apply(this, arguments);

            // Add error handler for metadata failed
            this.metadataFailed().then(function () {
                MessageBox.error(this.getModel("i18n").getResourceBundle().getText("errorServiceUnavailable"), {
                    styleClass: this.getContentDensityClass()
                });
            }.bind(this));

            // set device model
            var oDeviceModel = Models.createDeviceModel();
            this.setModel(oDeviceModel, "device");

            // create the views based on the url/hash
            this.getRouter().initialize();

            // set on behalf dialog
            this._onBehalfDialog = new OnBehalfDialog(this.getRootControl());

            // Register business suite icons
            IconPool.registerFont({
                fontFamily: "BusinessSuiteInAppSymbols",
                fontURI: sap.ui.require.toUrl("sap/ushell/themes/base/fonts")
            });
        },

        /**
         * Is called when the application is closed
         * @memberOf porsche.cats.timerecording.Component
         */
        onExit: function () {
            UIComponent.prototype.onExit.apply(this, arguments);
        },

        /**
         * Destroys the application after exiting
         * @memberOf porsche.cats.timerecording.Component
         */
        destroy: function () {
            // call the base component's destroy function
            UIComponent.prototype.destroy.apply(this, arguments);
        },

        /**
         * Destroys the dialog when exiting
         * @memberOf porsche.cats.timerecording.Component
         */
        exit: function () {

            //this._onBehalfDialog.exit();

            this._onBehalfDialog.destroy();
            delete this._onBehalfDialog;
        },

        /**
         * Opens the on behalf dialog
         * @memberOf porsche.cats.timerecording.Component
         */
        openOnBehalfDialog: function () {
            this._onBehalfDialog.open();
        },

        /**
         * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
         * design mode class should be set, which influences the size appearance of some controls
         * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
         * @memberOf porsche.cats.timerecording.Component
         */
        getContentDensityClass: function () {
            if (!this._sContentDensityClass) {
                if (!Device.support.touch) {
                    this._sContentDensityClass = "sapUiSizeCompact";
                } else {
                    this._sContentDensityClass = "sapUiSizeCozy";
                }
            }
            return this._sContentDensityClass;
        },

        /**
         * This method checks if the metadata loading fails
         * @returns {boolean} Indicator if metadata load failed
         * @memberOf porsche.cats.timerecording.Component
         */
        metadataFailed: function () {
            var oModel = this.getModel(); // from the descriptor
            var bFailedAlready = oModel.isMetadataLoadingFailed();
            return bFailedAlready ? Promise.resolve() : new Promise(function (fnResolve) {
                return oModel.attachEventOnce("metadataFailed", fnResolve);
            });
        }
    });
});
