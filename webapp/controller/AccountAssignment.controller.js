/**
 * Created by mdeppe on 25.10.2019.
 */
sap.ui.define([
    "porsche/cats/timerecording/controller/BaseController",
    "porsche/cats/timerecording/util/Models",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/model/Sorter"

], function (BaseController, Models, Filter, Fragment, Sorter) {  // eslint-disable-line id-match
    "use strict";

    var aSorters = [];
    var aInputsToValidate = [
        "inpCostObjectId",
        "inpAssignedHours"
    ];

    // noinspection UnnecessaryLocalVariableJS
    /**
     * AccountAssignment.controller.js
     *
     * This is the left area of the layout. It contains the main navigation area.
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {object} [mSettings] initial settings for the new control
     *
     * @class AccountAssignment.controller.js
     *
     * @extends sap.ui.core.mvc.Controller
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.AccountAssignment
     */
    var oAccountAssignment = BaseController.extend("porsche.cats.timerecording.controller.AccountAssignment", {

        /* =========================================================== */
        /* Standard functions 										   */
        /* =========================================================== */

        /**
         * This function is called every time the view is loaded
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onInit: function () {
            this.component = this.getOwnerComponent();
            this.bus = this.component.getEventBus();

            // Initialize message model
            var oMessageManager = sap.ui.getCore().getMessageManager();
            this.getView().setModel(oMessageManager.getMessageModel(), "message");

            // Initialize header model
            var oHeaderModel = Models.createAccountAssignmentHeaderModel();
            this.getView().setModel(oHeaderModel, "header");

            // Activate automatic message generation for complete view
            oMessageManager.registerObject(this.getView(), true);

            // Register events on event bus
            this.bus.subscribe("refreshData", "afterTimeRecordingChange", this._refreshAfterTimeRecordingChange, this);

            this.getRouter().getRoute("home").attachMatched(this._onHomeRouteMatched, this);
            this.getRouter().getRoute("timeRange").attachMatched(this._onTimeRangeRouteMatched, this);

            // Time management routes
            this.getRouter().getRoute("timeManagementHome").attachMatched(this._onHomeRouteMatched, this);
            this.getRouter().getRoute("timeManagementTimeRange").attachMatched(this._onTimeRangeRouteMatched, this);


            // Handle resize events of table for responsive behavior
            var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments");
            oTable.addEventDelegate({
                onAfterRendering: this.onTableResized.bind(this)
            });

        },

//      onBeforeRendering: function() {},

//      onAfterRendering: function() {},

        /**
         * This function is called every time the view is exited
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onExit: function () {
            this._unbindView();
        },

        /* =========================================================== */
        /* Utility functions of controller  						   */
        /* =========================================================== */

        /**
         * Checks the input fields on this view for validity
         * @returns {boolean} Indicator for the validity of the view
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        isValidView: function () {

            var bValid = true;

            // Check all fields on view
            jQuery.each(aInputsToValidate, function (iIndex, sId) {

                var oInput = this.byId(sId);

                if (oInput) {
                    // Check this input for validity
                    if (!this.validateInput(oInput)) {
                        bValid = false;
                    }
                }

            }.bind(this));

            return bValid;

        },

        /* =========================================================== */
        /* Event handlers (starts with "on")						   */
        /* =========================================================== */

        /**
         * Opens the sorter settings dialog
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onOpenSortSettings: function () {

            var oView = this.getView();

            // Instantiate the sort dialog
            if (!this.byId("vsdAccountAssignmentSortDialog")) {
                // load asynchronous XML fragment
                Fragment.load({
                    id: oView.getId(),
                    controller: this,
                    name: this.getDialogPath() + "AccountAssignmentsSortDialog"
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    oView.addDependent(oDialog);
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    this.byId("vsdAccountAssignmentSortDialog").open();
                }.bind(this));
            } else {
                this.byId("vsdAccountAssignmentSortDialog").open();
            }
        },

        /**
         * Closes the Sort dialog and apply sorter to the table
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onConfirmSorting: function (oEvent) {
            var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments"),
                bDescending = oEvent.getParameter("sortDescending"),
                oBinding = oTable.getBinding("items"),
                sPath = oEvent.getParameter("sortItem").getKey();

            aSorters = [];
            aSorters.push(new Sorter(sPath, bDescending));

            // apply the selected sort settings
            oBinding.sort(aSorters);
        },

        /**
         * Opens a dialog to edit the account assignment element
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @param {string} sSourceControl - The source control from which the event was raised
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onEditAccountAssignmentElement: function (oEvent, sSourceControl) {

            var oView = this.getView(),
                sBindingPath;

            // In case of mobile mode, get binding path from selected item
            if (sSourceControl === "toolbar") {
                var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments");
                sBindingPath = oTable.getSelectedContextPaths()[0];
                // Check if an entry was chosen from table
                if (!sBindingPath) {
                    sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
                    return;
                }
                // Check if the entry is editable
                if (!oTable.getSelectedContexts()[0].getProperty("Editable")) {
                    sap.m.MessageToast.show(this.getResourceText("entryNotEditable"));
                    return;
                }
            } else {
                sBindingPath = oEvent.getSource().getBindingContext().getPath();
            }

            // Instantiate the sort dialog<
            if (!this.byId("dlgAccountAssignmentEdit")) {
                // Load asynchronous XML fragment
                Fragment.load({
                    id: oView.getId(),
                    controller: this,
                    name: this.getDialogPath() + "AccountAssignmentEditDialog"
                }).then(function (oDialog) {
                    // Connect dialog to the view
                    oView.addDependent(oDialog);
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.bindElement({
                        path: sBindingPath
                    });

                    // Prevent decimal input field from wrong key uses
                    var oInput = this.byId("inpAssignedHours");
                    if (oInput) {
                        oInput.attachBrowserEvent("keydown", function (oKeyEvent) {
                            var oEvt = oKeyEvent || event;         // eslint-disable-line id-match
                            var sChar = oEvt.key;
                            if (/^[^\d,-]$/gi.test(sChar)) {
                                oEvt.preventDefault();
                            }
                        });

                        // Select all text when focusing into the field
                        oInput.attachBrowserEvent("focusin", function () {
                            oInput.selectText(0, oInput.getValue().length);
                        });

                        // Add input type attribute to html element to use numeric keyboard on mobile devices
                        oInput.addEventDelegate({
                            onAfterRendering: this.addDecimalInputMode
                        });
                    }

                    // Add input type for cost assignment, too
                    var oCostAssignmentObject = this.byId("inpCostObjectId");
                    if (oCostAssignmentObject) {
                        oCostAssignmentObject.addEventDelegate({
                            onAfterRendering: this.addDecimalInputMode
                        });
                    }

                    // Add input type for loan cost center, too
                    var oLoanCostCenter = this.byId("inpLoanCostCenter");
                    if (oLoanCostCenter) {
                        oLoanCostCenter.addEventDelegate({
                            onAfterRendering: this.addDecimalInputMode
                        });
                    }

                    oDialog.open();
                }.bind(this));
            } else {
                var oDialog = this.byId("dlgAccountAssignmentEdit");
                oDialog.bindElement({
                    path: sBindingPath
                });
                oDialog.open();
            }

        },

        /**
         * Closes the sort-order Dialog and destroys it
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onAfterCloseAccountAssignmentEditDialog: function (oEvent) {

            var oModel = this.getView().getModel();

            // Check for pending changes and revert them
            if (oModel.hasPendingChanges()) {
                oModel.resetChanges();
            }

            oEvent.getSource().unbindElement();

        },

        /**
         * Saves the changes on the account assignment dialog and closes it afterwards
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onSaveAccountAssignmentEdit: function (oEvent) {

            var oDialog = this.byId("dlgAccountAssignmentEdit");
            oDialog.setBusy(true);

            // Save changes made
            this.saveChanges({
                isExplicitSave: true,
                success: function (oContext) {      // eslint-disable-line require-jsdoc

                    oDialog.setBusy(false);
                    oDialog.close();

                    // Refresh all necessary data after changing of a time record
                    this.bus.publish("refreshData", "afterTimeRecordingChange");

                }.bind(this),
                error: function () {        // eslint-disable-line require-jsdoc
                    // Leave dialog open
                    oDialog.setBusy(false);
                },
                isCreate: false
            });

        },

        /**
         * Closes the dialog when the user presses the cancel button
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onCancelAccountAssignmentEdit: function (oEvent) {

            this.cancelChanges({
                success: function () {
                    // Close the Dialog after reverting all changes
                    this.byId("dlgAccountAssignmentEdit").close();
                }.bind(this),
                showSecurityPopover: true,
                popoverSourceControl: oEvent.getSource(),
                popoverPlacement: "PreferredBottomOrFlip"
            });

        },

        /**
         * Deletes the account assignment element after showing a popover for security
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @param {string} sSourceControl - The source control from which the event was raised
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onDeleteAccountAssignmentElement: function (oEvent, sSourceControl) {

            var oContext;

            // In case of mobile mode, get binding path from selected item
            if (sSourceControl === "toolbar") {
                var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments");
                oContext = oTable.getSelectedContexts()[0];
                // Check if an entry was chosen from the table
                if (!oContext) {
                    sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
                    return;
                }
                // Check if the entry is editable
                if (!oContext.getProperty("Editable")) {
                    sap.m.MessageToast.show(this.getResourceText("entryNotEditable"));
                    return;
                }
            } else {
                oContext = oEvent.getSource().getBindingContext();
            }

            this.deleteEntity({
                path: oContext.getPath(),
                showSecurityPopover: true,
                popoverSourceControl: oEvent.getSource(),
                popoverPlacement: "PreferredBottomOrFlip",
                popoverQuestion: "questionDelete",
                success: function () {          // eslint-disable-line require-jsdoc
                    // Refresh data
                    this.bus.publish("refreshData", "afterTimeRecordingChange");
                }.bind(this)

            });

        },

        /**
         * Handles the selection of cost object in the search help
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onSelectCostObjectFromValueHelp: function (oEvent) {

            var oSelectedContext = oEvent.getParameter("listItem").getBindingContext();

            if (oSelectedContext) {
                var oDialog = this.byId("dlgAccountAssignmentEdit"),
                    oDialogContext = oDialog.getBindingContext();

                if (oDialogContext) {

                    // Fill in value into correct field
                    if (oEvent.getSource().getModel("dialog").getProperty("/isLoanCostCenter")) {
                        this.getModel().setProperty("LoanCostCenter", oSelectedContext.getProperty("CostObjectID"), oDialogContext);
                    } else {
                        this.getModel().setProperty("CostObjectID", oSelectedContext.getProperty("CostObjectID"), oDialogContext);
                    }

                }
            }

            var oSearchHelp = this.byId("dlgSearchHelp");
            if (oSearchHelp) {
                oSearchHelp.close();
            }
        },

        /**
         * Navigates back to master view in phone mode
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onNavToMasterOnSmallDevice: function () {
            this.getModel("layoutModel").setProperty("/layout", "OneColumn");
        },

        /**
         * This method is called every time the table do's a re-rendering (f.e. because of resizing the view)
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onTableResized: function (oEvent) {

            // Get Column with action buttons
            var oColumnActionButton = this.byId("frgAccountAssignmentTable--clmActionButtons"),
                oColumnHoursTransferred = this.byId("frgAccountAssignmentTable--clmHoursTransferred"),
                bColumnActionButtonVisible = oColumnActionButton._media.matches,
                bColumnHoursTransferredVisible = oColumnHoursTransferred._media.matches;

            this.getModel("table").setProperty("/actionButtonsVisibleOnAccountAssignmentTable", bColumnActionButtonVisible);
            this.getModel("table").setProperty("/hoursTransferredVisible", bColumnHoursTransferredVisible);
        },

        /* =========================================================== */
        /* Private functions (starts with "_")						   */
        /* =========================================================== */

        /**
         * This method is called every time the route was matched
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         * @private
         */
        _onHomeRouteMatched: function (oEvent) {

            var sRoute = oEvent.getParameter("name").toString(),
                oArguments = oEvent.getParameter("arguments");

            // Wait for personal number
            this.getGeneralInfoModelPromise().then(function (aData) {

                // Adjust view model due to time management mode
                if (sRoute === "timeManagementHome") {
                    var sPernr = oArguments.PersonalNumber;
                    this.getModel("view").setProperty("/timeManagement", true);
                    this.getModel("view").setProperty("/pernr", sPernr);
                } else {
                    // Disable time management mode
                    this.getModel("view").setProperty("/timeManagement", false);
                    this.getModel("view").setProperty("/pernr", this.getModel("generalInfo").getProperty("/Pernr"));
                }

                this._unbindView();

                // Rebind page binding
                this._rebindPageHeader(this.getModel("view").getProperty("/pernr"));

            }.bind(this));

        },

        /**
         * This method is called every time the route was matched
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.AccountAssignment
         * @private
         */
        _onTimeRangeRouteMatched: function (oEvent) {

            var sRoute = oEvent.getParameter("name").toString(),
                oArguments = oEvent.getParameter("arguments");

            // Adjust view model due to time management mode
            if (sRoute === "timeManagementTimeRange") {
                this.getModel("view").setProperty("/timeManagement", true);
            } else {
                // Disable time management mode
                this.getModel("view").setProperty("/timeManagement", false);
            }

            this.getModel("view").setProperty("/pernr", oArguments.PersonalNumber);

            // Show this view in normal mode
            if (this.getModel("layoutModel").getProperty("/layout") === "OneColumn") {
                var sColumnsMode = this.getModel("layoutModel").getProperty("/smallScreenMode") ? "OneColumn" : "TwoColumnsMidExpanded";
                this.getModel("layoutModel").setProperty("/layout", sColumnsMode);
                this.getModel("layoutModel").setProperty("/forcedLayout", false);
            }

            // It's necessary to format the date as UTC, because otherwise the filter will request the wrong date
            this.oStartDate = this.Formatter.formatAsUtcDate(new Date(oArguments.StartDate));
            this.oEndDate = this.Formatter.formatAsUtcDate(new Date(oArguments.EndDate));

            // Update header model with selected date
            this.getModel("header").setProperty("/StartDate", this.oStartDate);
            // Only set end date, when it's different from start date
            if (this.oStartDate.getTime() !== this.oEndDate.getTime()) {
                this.getModel("header").setProperty("/EndDate", this.oEndDate);
            } else {
                this.getModel("header").setProperty("/EndDate", null);
            }

            // Bind this time range to the table
            this._buildTableForSelectedDates();

            // Rebind page binding
            this._rebindPageHeader(oArguments.PersonalNumber);
        },

        /**
         * Adjusts the binding of the account assignment elements table
         * @memberOf porsche.cats.timerecording.AccountAssignment
         * @private
         */
        _buildTableForSelectedDates: function () {

            var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments");

            aSorters.push(new Sorter("CostObjectID", false));

            oTable.bindAggregation("items", {
                path: "/CostAssignmentSet",
                filters: [
                    new Filter({
                        path: "Pernr",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.getModel("view").getProperty("/pernr")
                    }),
                    new Filter({
                        path: "CaleSelDateBegin",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.oStartDate
                    }),
                    new Filter({
                        path: "CaleSelDateEnd",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.oEndDate
                    })
                ],
                sorter: aSorters,
                template: this.byId("frgAccountAssignmentTable--cliAccountAssignmentElement"),
                events: {
                    dataReceived: function (oResponse) {     // eslint-disable-line require-jsdoc

                        // (Re-) calculate total hours of all entries
                        var oData = oResponse.getParameter("data");
                        if (oData && oData.results && oData.results.length > 0) {
                            this._handleAccountAssignmentItems(oData.results);
                        } else {
                            this.getModel("header").setProperty("/TotalHours", 0.00);
                        }
                        oTable.setBusy(false);
                    }.bind(this),
                    dataRequested: function () {    // eslint-disable-line require-jsdoc
                        oTable.setBusy(true);
                    }
                }
            });
        },

        /**
         * Rebinds the page for the current user
         * @param {string} [sPernr] The personal number of the current user for recording
         * @memberOf porsche.cats.timerecording.AccountAssignment
         * @private
         */
        _rebindPageHeader: function (sPernr) {
            var oPage = this.byId("pgAccountAssignment"),
                oModel = this.getModel(),
                sBindingPath = oModel.createKey("GeneralInfoSet", {
                    Pernr: sPernr
                });

            oPage.bindElement({
                path: "/" + sBindingPath
            });
        },

        /**
         * Unbinds all relevant elements from this view
         * @memberOf porsche.cats.timerecording.AccountAssignment
         * @private
         */
        _unbindView: function () {

            // Unbind table items
            var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments");
            oTable.unbindAggregation("items");

            // Remove variables
            delete this.oStartDate;
            delete this.oEndDate;
            aSorters = [];

            // Reset sort dialog
            var oSortDialog = this.byId("vsdAccountAssignmentSortDialog");
            if (oSortDialog) {
                oSortDialog.destroy();
            }

            // Reset edit dialog
            var oEditDialog = this.byId("dlgAccountAssignmentEdit");
            if (oEditDialog) {
                oEditDialog.destroy();
            }

            // Reset header model
            this.getView().setModel(Models.createAccountAssignmentHeaderModel(), "header");
        },

        /**
         * Channel event handler for event "selectCategory" on KPI overview
         * @param {string} [sChannel] The channel of the event to fire
         * @param {string} [sEvent] The identifier of the event to fire
         * @param {Object} [oData] The parameters which should be carried by the event
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _refreshAfterTimeRecordingChange: function (sChannel, sEvent, oData) {

            // Refresh table items binding
            var oTable = this.byId("frgAccountAssignmentTable--tblAccountAssignments");
            oTable.getBinding("items").refresh(true);
        },

        /**
         * Calculates the total hours of all account assignment elements and updates it in the model
         * Also, check if one of the items has a loan cost center
         * @param {array} [aElements] An array with the account assignment elements
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _handleAccountAssignmentItems: function (aElements) {

            var fTotalHours = 0.00,
                bIncludesItemWithLoanCostCenter = false;

            jQuery.each(aElements, function (iIndex, oElement) {
                fTotalHours += parseFloat(oElement.AssignedHours);
                if (oElement.LoanCostCenter) {
                    bIncludesItemWithLoanCostCenter = true;
                }
            });

            this.getModel("header").setProperty("/TotalHours", fTotalHours);
            this.getModel("header").setProperty("/ShowLoanCostCenterColumn", bIncludesItemWithLoanCostCenter);

        }

    });

    return oAccountAssignment;

});
