/**
 * Created by mdeppe on 25.10.2019.
 */
sap.ui.define([
    "porsche/cats/timerecording/controller/BaseController",
    "porsche/cats/timerecording/util/Formatter",
    "porsche/cats/timerecording/util/Models",
    "sap/ui/core/Fragment",
    "sap/ui/unified/DateRange",
    "sap/ui/unified/DateTypeRange",
    "sap/ui/model/json/JSONModel"

], function (BaseController, Formatter, Models, Fragment, DateRange, DateTypeRange, JSONModel) {  // eslint-disable-line id-match
    "use strict";

    var aInputsToValidate = [
        "frgTimeRecordingForm--inpAssignedHours",
        "frgTimeRecordingForm--inpCostObjectId"
    ];
    var aDecInputs = [
        "frgTimeRecordingForm--inpAssignedHours",
        "frgTimeRecordingForm--inpAssignedHoursPercentage"
    ];
    var aIntInputs = [];

    var oCalendarPromise;

    // noinspection UnnecessaryLocalVariableJS
    /**
     * Master.controller.js
     *
     * This is the left area of the layout. It contains the main navigation area.
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {object} [mSettings] initial settings for the new control
     *
     * @class Master.controller.js
     *
     * @extends porsche.cats.timerecording.controller.BaseController
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.Master
     */
    var oMaster = BaseController.extend("porsche.cats.timerecording.controller.Master", {

        /* =========================================================== */
        /* Standard functions 										   */
        /* =========================================================== */

        /**
         * This function is called every time the view is loaded
         * @memberOf porsche.cats.timerecording.Master
         */
        onInit: function () {
            this.component = this.getOwnerComponent();
            this.bus = this.component.getEventBus();

            // Initialize message model
            var oMessageManager = sap.ui.getCore().getMessageManager();
            this.getView().setModel(oMessageManager.getMessageModel(), "message");

            // Activate automatic message generation for complete view
            oMessageManager.registerObject(this.getView(), true);

            // Create message popover
            this.createMessagePopover();

            // Register events on event bus
            this.bus.subscribe("refreshData", "afterTimeRecordingChange", this._refreshAfterTimeRecordingChange, this);
            this.bus.subscribe("waitFor", "settingsModel", this._checkCalendarSelectionAfterSettingsReload, this);

            this.getRouter().getRoute("home").attachMatched(this._onHomeRouteMatched, this);
            this.getRouter().getRoute("timeRange").attachMatched(this._onTimeRangeRouteMatched, this);

            // Time Management routes
            this.getRouter().getRoute("timeManagementHome").attachMatched(this._onHomeRouteMatched, this);
            this.getRouter().getRoute("timeManagementTimeRange").attachMatched(this._onTimeRangeRouteMatched, this);

            // Add some browser event handlers to input fields
            jQuery.each(aDecInputs, function (i, sDecInput) {
                var oInput = this.byId(sDecInput);
                if (oInput) {
                    // Prevent decimal input field from wrong key uses
                    oInput.attachBrowserEvent("keydown", function (oEvent) {
                        var oEvt = oEvent || event;         // eslint-disable-line id-match
                        var sChar = oEvt.key;
                        if (/^[^\d,-]$/gi.test(sChar)) {
                            oEvt.preventDefault();
                        }
                    });
                    // Select all text when focusing into the field
                    oInput.attachBrowserEvent("focusin", function (oEvent) {
                        oInput.selectText(0, oInput.getValue().length);
                    });
                    // Add input type attribute to html element to use numeric keyboard on mobile devices
                    oInput.addEventDelegate({
                        onAfterRendering: this.addDecimalInputMode
                    });
                }
            }.bind(this));
            jQuery.each(aIntInputs, function (i, sIntInput) {
                var oInput = this.byId(sIntInput);
                if (oInput) {
                    // Prevent integer input field from wrong key uses
                    oInput.attachBrowserEvent("keydown", function (oEvent) {
                        var oEvt = oEvent || event;         // eslint-disable-line id-match
                        var sChar = oEvt.key;
                        if (/^[^\d-]$/gi.test(sChar)) {
                            oEvt.preventDefault();
                        }
                    });
                    // Select all text when focusing into the field
                    oInput.attachBrowserEvent("focusin", function (oEvent) {
                        oInput.selectText(0, oInput.getValue().length);
                    });
                }
            }.bind(this));

            // Add decimal input mode for cost object input, too (for mobile devices)
            var oCostObjectInput = this.byId("frgTimeRecordingForm--inpCostObjectId");
            if (oCostObjectInput) {
                oCostObjectInput.addEventDelegate({
                    onAfterRendering: this.addDecimalInputMode
                });
            }

        },

//      onBeforeRendering: function() {},

//      onAfterRendering: function() {},

        /**
         * This function is called every time the view is exited
         * @memberOf porsche.cats.timerecording.Master
         */
        onExit: function () {
            this._destroyMessagePopover();

            // Destroy calendar legend popover
            if (this._oLegendPopover) {
                this._oLegendPopover.destroy();
            }
        },

        /* =========================================================== */
        /* Utility functions of controller  						   */
        /* =========================================================== */

        /**
         * Checks the input fields on this view for validity
         * @returns {boolean} Indicator for the validity of the view
         * @memberOf porsche.cats.timerecording.Master
         */
        isValidView: function () {

            var bValid = true;

            // Check all fields on view
            jQuery.each(aInputsToValidate, function (iIndex, sId) {

                var oInput = this.byId(sId);

                if (oInput) {
                    // Check this input for validity
                    if (!this.validateInput(oInput)) {
                        bValid = false;
                    }
                }

            }.bind(this));

            return bValid;

        },

        /**
         * Determines if the message strip warning for loan cost centers shall be shown
         * @param {boolean} [bUseLoanCostCenter] Indicator if the loan cost center is enabled for usage
         * @param {integer} [iHoursRemaining] The amount of remaining hours
         * @param {boolean} [bFormIsEnabled] Indicator if the form is enabled
         * @param {boolean} [aLoanCostCenters] The collection of loan cost centers configured in the settings
         * @returns {boolean} Indicator if the message strip shall be shown
         * @memberOf porsche.cats.timerecording.Master
         */
        formatLoanCostCenterWarningVisibility: function (bUseLoanCostCenter, iHoursRemaining, bFormIsEnabled, aLoanCostCenters) {
            return !!(bUseLoanCostCenter && iHoursRemaining > 0 && bFormIsEnabled && aLoanCostCenters.length < 2);
        },

        /* =========================================================== */
        /* Event handlers (starts with "on")						   */
        /* =========================================================== */

        /**
         * This method is triggered every time a date selection was changed.
         * It navigates to a new route, using the parameters for the chosen selection.
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onTimeIntervalSelected: function (oEvent) {

            var aSelectedDates = oEvent.getSource().getSelectedDates();

            // Check if a date selection was taken
            if (aSelectedDates.length > 0) {
                var oStartDate = aSelectedDates[0].getProperty("startDate"),
                    // If single selection was chosen, use start date for end date
                    bSingleSelection = this.getModel("view").getProperty("/selectionMode") === "single",
                    oEndDate = bSingleSelection ? aSelectedDates[0].getProperty("startDate") : aSelectedDates[0].getProperty("endDate");

                // Check if both start and end date is provided
                if (oStartDate && oEndDate) {
                    var oRouter = this.getRouter();

                    // Check if time management mode is enabled
                    if (this.getModel("view").getProperty("/timeManagement")) {
                        oRouter.navTo("timeManagementTimeRange", {
                            PersonalNumber: this.getModel("view").getProperty("/pernr"),
                            StartDate: oStartDate.toString(),
                            EndDate: oEndDate.toString()
                        });
                    } else {
                        oRouter.navTo("timeRange", {
                            PersonalNumber: this.getModel("view").getProperty("/pernr"),
                            StartDate: oStartDate.toString(),
                            EndDate: oEndDate.toString()
                        });
                    }


                }
            }
        },

        /**
         * This method is triggered every time the selection (day or interval) changes
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onChangeCalendarSelection: function (oEvent) {
            this.byId("calRecordingCalendar").destroySelectedDates();
        },

        /**
         * Resets the form and time selection of the calendar
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onCancelTimeRecording: function (oEvent) {

            this.cancelChanges({
                success: function () {

                    if (this.getModel("view").getProperty("/timeManagement")) {
                        // In case of time management mode, move back to time management home route
                        this.getRouter().navTo("timeManagementHome", {
                            PersonalNumber: this.getModel("view").getProperty("/pernr")
                        });
                    } else {
                        // Navigates back to home view (=reset calendar etc)
                        this.getRouter().navTo("home");
                    }
                }.bind(this),
                showSecurityPopover: true,
                popoverSourceControl: oEvent.getSource(),
                popoverPlacement: "PreferredBottomOrFlip"
            });

        },

        /**
         * Saves the time recording, calling a function import
         * @memberOf porsche.cats.timerecording.Master
         */
        onCreateTimeRecording: function () {

            var oModel = this.getModel(),
                oForm = this.byId("frgTimeRecordingForm--frmTimeRecording"),
                oRecordModel = this.getModel("recording"),
                oParameters = {
                    AssignedHours: oRecordModel.getProperty("/AssignedHours"),
                    CostObjectID: oRecordModel.getProperty("/CostObjectID"),
                    CostObjectType: oRecordModel.getProperty("/CostObjectType"),
                    CaleSelDateEnd: oRecordModel.getProperty("/CaleSelDateEnd"),
                    CaleSelDateBegin: oRecordModel.getProperty("/CaleSelDateBegin"),
                    Pernr: oRecordModel.getProperty("/Pernr"),
                    TotalHours: oRecordModel.getProperty("/TotalHours"),
                    AdditionalHours: oRecordModel.getProperty("/AdditionalHours"),
                    LoanCostCenter: oRecordModel.getProperty("/LoanCostCenter"),
                    CaseNumber: oRecordModel.getProperty("/CaseNumber")
                };

            // Check validity of necessary fields
            if (this._isValid()) {

                oForm.setBusy(true);

                // Remove messages from message manager
                var oMessageManager = sap.ui.getCore().getMessageManager();
                oMessageManager.removeAllMessages();

                // Call function import
                oModel.callFunction("/UpdateCostAssignment", {
                    method: "GET",
                    urlParameters: oParameters,
                    success: function (oData, oResponse) {     // eslint-disable-line require-jsdoc
                        // Check if saving was successful
                        if (oResponse.headers.return_code === "S") {
                            this.bus.publish("refreshData", "afterTimeRecordingChange");
                        }
                        oForm.setBusy(false);
                    }.bind(this),
                    error: function (oResponse) {       // eslint-disable-line require-jsdoc
                        // In case of an syntax error, throw a message manually
                        if (oResponse.statusCode.toString().substr(0, 1) === "5") {
                            this.addMessageToMessageManager({
                                message: oResponse.message,
                                additionalText: oResponse.statusText,
                                code: oResponse.statusCode,
                                description: oResponse.responseText,
                                technical: true,
                                type: "Error"
                            });
                        }

                        oForm.setBusy(false);
                    }.bind(this)
                });
            }

        },

        /**
         * Is called every time the specialDates aggregation receives new data from backend
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onDaysReceived: function (oEvent) {

            var oData = oEvent.getParameter("data"),
                oCalendar = this.byId("calRecordingCalendar");

            // Remove all disabled days first
            oCalendar.removeAllDisabledDates();

            if (oData && oData.results && oData.results.length > 0) {

                // Check all days
                jQuery.each(oData.results, function (iIndex, oDay) {

                    // If this day is a non working day, add it to the special dates using type NonWorking
                    if (!oDay.Workingday) {
                        oCalendar.addSpecialDate(new DateTypeRange({
                            startDate: new Date(oDay.CaleDate),
                            type: sap.ui.unified.CalendarDayType.NonWorking
                        }));
                    }

                });
            }

        },

        /**
         * Handles the selection of cost object in the search help
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onSelectCostObjectFromValueHelp: function (oEvent) {

            var oSelectedContext = oEvent.getParameter("listItem").getBindingContext();

            if (oSelectedContext) {
                var oRecordModel = this.getModel("recording");

                // Fill in value into correct field
                if (oEvent.getSource().getModel("dialog").getProperty("/isLoanCostCenter")) {
                    oRecordModel.setProperty("/LoanCostCenter", oSelectedContext.getProperty("CostObjectID"));
                } else {
                    oRecordModel.setProperty("/CostObjectID", oSelectedContext.getProperty("CostObjectID"));
                    oRecordModel.setProperty("/CostObjectType", oSelectedContext.getProperty("CostObjectType"));
                }
            }

            var oSearchHelp = this.byId("dlgSearchHelp");
            if (oSearchHelp) {
                oSearchHelp.close();
            }
        },

        /**
         * Calculates the percentage value for assigned hours, depending of the absolute value
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onAssignedHoursChange: function (oEvent) {
            var fValue = parseFloat(oEvent.getParameter("value").replace(",", ".")),
                oRecordingModel = this.getModel("recording"),
                fPercentageValue = fValue / oRecordingModel.getProperty("/TotalHours") * 100;

            oRecordingModel.setProperty("/Percentage", fPercentageValue);
        },

        /**
         * Calculates the absolute value for assigned hours, depending of the percentage
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onPercentageHoursChange: function (oEvent) {
            var fValue = parseFloat(oEvent.getParameter("value").replace(",", ".")),
                oRecordingModel = this.getModel("recording"),
                fHours = oRecordingModel.getProperty("/TotalHours") * fValue / 100;

            oRecordingModel.setProperty("/AssignedHours", fHours);
        },

        /**
         * Navigates to account assignment view in phone mode
         * @memberOf porsche.cats.timerecording.Master
         */
        onNavToAccountAssignmentsOnSmallDevice: function () {
            this.getModel("layoutModel").setProperty("/layout", "TwoColumnsMidExpanded");
        },

        /**
         * Open Legend on smaller devices
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onOpenLegend: function (oEvent) {
            var oButton = oEvent.getSource();

            if (!this._oLegendPopover) {
                Fragment.load({
                    name: this.getFragmentPath() + "CalendarLegend",
                    controller: this
                }).then(function (oPopover) {
                    this._oLegendPopover = oPopover;
                    this.getView().addDependent(this._oLegendPopover);
                    this._oLegendPopover.openBy(oButton);
                }.bind(this));
            } else {
                this._oLegendPopover.openBy(oButton);
            }
        },

        /**
         * Close the calendar legend
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         */
        onCloseLegend: function (oEvent) {
            this._oLegendPopover.close();
        },

        /**
         * Navigates to the user settings page
         * @memberOf porsche.cats.timerecording.Master
         */
        onNavToUserSettings: function () {
            this.getRouter().navTo("userSettings");
        },

        /**
         * Opens an info link in a new browser tab
         * @memberOf porsche.cats.timerecording.Master
         */
        onOpenInfoLink: function () {
            var sHelpLinkUrl = this.getView().getBindingContext().getProperty("Url");

            // Open the url in a new browser tab
            if (sHelpLinkUrl) {
                var oNewTab = window.open(sHelpLinkUrl, "_blank");
                if (oNewTab && typeof oNewTab.focus === "function") {
                    oNewTab.focus();
                } else {
                    //show error message, if popups blocked from browser
                    sap.m.MessageToast.show(this.getResourceText("warningPopupBlocked"));
                }
            }
        },

        /**
         * Opens a dialog for the time manager to chose an user for on behalf recording
         * @memberOf porsche.cats.timerecording.Master
         */
        onOpenOnBehalfDialog: function () {
            this.getOwnerComponent().openOnBehalfDialog();
        },

        /**
         * Returns to the own recording route
         * @memberOf porsche.cats.timerecording.Master
         */
        onReturnToOwnRecording: function () {
            this.getRouter().navTo("home");
        },

        /* =========================================================== */
        /* Private functions (starts with "_")						   */
        /* =========================================================== */

        /**
         * This method is called every time the route was matched
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _onHomeRouteMatched: function (oEvent) {

            var sRoute = oEvent.getParameter("name").toString(),
                oArguments = oEvent.getParameter("arguments");

            // Wait for personal number
            this.getGeneralInfoModelPromise().then(function (aData) {

                // Adjust view model due to time management mode
                if (sRoute === "timeManagementHome") {
                    var sPernr = oArguments.PersonalNumber;
                    this.getModel("view").setProperty("/timeManagement", true);
                    this.getModel("view").setProperty("/pernr", sPernr);
                } else {
                    // Disable time management mode
                    this.getModel("view").setProperty("/timeManagement", false);
                    this.getModel("view").setProperty("/pernr", this.getModel("generalInfo").getProperty("/Pernr"));
                }

                // Show this view in normal mode
                if (this.getModel("layoutModel").getProperty("/smallScreenMode")) {
                    this.getModel("layoutModel").setProperty("/layout", "OneColumn");
                    this.getModel("layoutModel").setProperty("/forcedLayout", false);
                } else if (this.getModel("layoutModel").getProperty("/layout") === "OneColumn") {
                    this.getModel("layoutModel").setProperty("/layout", "TwoColumnsMidExpanded");
                }

                // Disable form and buttons
                this.getModel("view").setProperty("/enabled", false);

                // Write current route into view model
                this.getModel("view").setProperty("/route", "home");

                // Bind calender settings
                this._bindCalendarSettings();

                // Reset all necessary controls
                this._resetCalendarSelection();
                this._resetForm();
                this._resetValidation();

            }.bind(this));

        },

        /**
         * This method is called every time the route was matched
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _onTimeRangeRouteMatched: function (oEvent) {

            var sRoute = oEvent.getParameter("name").toString(),
                oArguments = oEvent.getParameter("arguments");

            // Wait for personal number
            this.getGeneralInfoModelPromise().then(function (aData) {

                // Adjust view model due to time management mode
                if (sRoute === "timeManagementTimeRange") {
                    this.getModel("view").setProperty("/timeManagement", true);
                } else {
                    // Disable time management mode
                    this.getModel("view").setProperty("/timeManagement", false);
                }

                this.getModel("view").setProperty("/pernr", oArguments.PersonalNumber);

                // Show this view in normal mode
                if (this.getModel("layoutModel").getProperty("/layout") === "OneColumn") {
                    var sColumnsMode = this.getModel("layoutModel").getProperty("/smallScreenMode") ? "OneColumn" : "TwoColumnsMidExpanded";
                    this.getModel("layoutModel").setProperty("/layout", sColumnsMode);
                    this.getModel("layoutModel").setProperty("/forcedLayout", false);
                }

                this.oStartDate = Formatter.formatAsUtcDate(new Date(oArguments.StartDate));
                this.oEndDate = Formatter.formatAsUtcDate(new Date(oArguments.EndDate));

                // If an interval is selection, assign the correct selection mode
                if (this.oStartDate.getTime() !== this.oEndDate.getTime()) {
                    this.getModel("view").setProperty("/selectionMode", "interval");
                }

                this._bindCalendarSettings(function () {
                    // Wait until the selection mode is set before we set the selection dates
                    this._checkCalendarSelection(this.oStartDate, this.oEndDate);
                    this._buildFormForSelectedDates(this.oStartDate, this.oEndDate);
                }.bind(this));

                // Enable form and buttons
                this.getModel("view").setProperty("/enabled", true);

                // Write current route into view model
                this.getModel("view").setProperty("/route", "reporting");

            }.bind(this));

        },

        /**
         * Checks for existing binding on calendar control
         * @memberOf porsche.cats.timerecording.Master
         * @param {function} [fnSuccess] Success handler method
         * @private
         */
        _bindCalendarSettings: function (fnSuccess) {

            var oModel = this.getModel(),
                oCalendar = this.byId("calRecordingCalendar"),
                oGeneralInfoModel = this.getModel("generalInfo"),
                oCaleNavDateMin = oGeneralInfoModel.getProperty("/MinDate"),
                oCaleNavDateMax = oGeneralInfoModel.getProperty("/MaxDate"),
                sPernr = oGeneralInfoModel.getProperty("/Pernr"),
                sBoundPernr;

            oCalendar.setBusy(true);

            // Check if time management mode is active and replace personal number in case of yes
            if (this.getModel("view").getProperty("/timeManagement")) {
                sPernr = this.getModel("view").getProperty("/pernr");
            }

            // Check if the calender is already bound for another user
            if (oCalendar.getBindingContext()) {
                sBoundPernr = oCalendar.getBindingContext().getProperty("Pernr");
            }

            // Check if binding is already available or if the personal number has been changed
            if (!oCalendar.getElementBinding() || sBoundPernr !== sPernr) {

                var sCalendarPath = oModel.createKey("CalendarSet", {
                    Pernr: sPernr,
                    CaleNavDateMin: Formatter.formatAsUtcDate(oCaleNavDateMin),
                    CaleNavDateMax: Formatter.formatAsUtcDate(oCaleNavDateMax)
                });

                // Bind the settings to the calendar
                oCalendarPromise = new Promise(function (fnResolve) {
                    oCalendar.bindElement({
                        path: "/" + sCalendarPath,
                        events: {
                            dataReceived: function (oEvent) {       // eslint-disable-line require-jsdoc
                                oCalendar.setBusy(false);
                                if (typeof fnSuccess === "function") {
                                    fnSuccess();
                                }
                                fnResolve();
                            }
                        }
                    });
                });


                // Add Binding for special dates
                oCalendar.bindAggregation("specialDates", {
                    path: "DaysSet",
                    length: 1000,
                    template: this.byId("dtrSpecialData"),
                    events: {
                        dataReceived: this.onDaysReceived.bind(this)
                    }
                });

                // Check if context is already loaded, then call fnSuccess instantly
                if (this.getModel().getProperty("/" + sCalendarPath)) {
                    oCalendar.setBusy(false);
                    if (typeof fnSuccess === "function") {
                        fnSuccess();
                    }
                }

            } else {

                oCalendar.setBusy(false);

                if (typeof fnSuccess === "function") {
                    fnSuccess();
                }
            }
        },

        /**
         * Checks if the calendar is selected with the correct dates (in case of new application instance)
         * @param {Date} [oStartDate] The first selected date (begin of interval)
         * @param {Date} [oEndDate] The second selected date (end of interval)
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _checkCalendarSelection: function (oStartDate, oEndDate) {

            var oCalendar = this.byId("calRecordingCalendar");

            if (oCalendar.getSelectedDates().length > 0) {

                var oSelectedStartDate = oCalendar.getSelectedDates()[0].getProperty("startDate"),
                    // If single selection was chosen, use start date for end date
                    bSingleSelection = this.getModel("view").getProperty("/selectionMode") === "single",
                    oSelectedEndDate = bSingleSelection ? oCalendar.getSelectedDates()[0].getProperty("startDate") : oCalendar.getSelectedDates()[0].getProperty("endDate");


                // If selected date is identical, no manual selection is necessary
                if (oSelectedStartDate.getTime() === oStartDate.getTime() && oSelectedEndDate.getTime() === oEndDate.getTime()) {
                    return;
                }
            }

            // Adjust selected date in case of no date is selected already (f.e. when new instance of application is loaded)
            oCalendar.removeAllSelectedDates();
            oCalendar.addSelectedDate(new DateRange({
                startDate: oStartDate,
                endDate: oEndDate
            }));
        },

        /**
         * Checks for correct selection of calendar selection mode after refreshing the settings model
         * @param {string} [sChannel] The channel of the event to fire
         * @param {string} [sEvent] The identifier of the event to fire
         * @param {Object} [oData] The parameters which should be carried by the event
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _checkCalendarSelectionAfterSettingsReload: function (sChannel, sEvent, oData) {

            // Set default selection mode from settings
            var oView = this.getView(),
                oViewModel = oView.getModel("view"),
                oSettingsModel = oData;

            // Get the user settings and apply the default selection method
            if (oSettingsModel) {
                oViewModel.setProperty("/selectionMode", (oSettingsModel.getProperty("/SelectionMethod") === "I") ? "interval" : "single");
            }
        },

        /**
         * Calculates the sum of target hours of the selected time interval and adjusts the recording form
         * @param {Date} [oStartDate] The first selected date (begin of interval)
         * @param {Date} [oEndDate] The second selected date (end of interval)
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _buildFormForSelectedDates: function (oStartDate, oEndDate) {

            // Reset form first
            this._resetForm();

            // Set busy state to form
            var oForm = this.byId("frgTimeRecordingForm--frmTimeRecording");
            oForm.setBusy(true);

            // Remove message for not editable dates from message manager
            this.removeMessageForTarget("notEditableDates");

            var oFormattedStartDate = oStartDate,
                oFormattedEndDate = oEndDate;

            // Update JSON model for creating new time records
            var oRecordModel = this.getModel("recording");
            oRecordModel.setProperty("/CaleSelDateBegin", oFormattedStartDate);
            oRecordModel.setProperty("/CaleSelDateEnd", oFormattedEndDate);
            oRecordModel.setProperty("/AssignedHours", null);
            oRecordModel.setProperty("/Percentage", null);
            oRecordModel.setProperty("/CostObjectID", "");
            oRecordModel.setProperty("/Pernr", this.getModel("view").getProperty("/pernr"));

            // Count maximal amount of hours, that can be assigned
            var oCalendar = this.byId("calRecordingCalendar"),
                oModel = this.getModel(),
                oSpecialDatesBinding = oCalendar.getBinding("specialDates"),
                oSpecialDatesPromise;

            // Check if binding is already available
            if (!oSpecialDatesBinding.bPendingRequest) {
                // If yes, transmit context directly to Promise
                oSpecialDatesPromise = oSpecialDatesBinding.getCurrentContexts();
            } else {
                // If no, wait for data from backend and transmit it to the Promise afterwards
                oSpecialDatesPromise = new Promise(function (fnResolve) {
                    oSpecialDatesBinding.attachEventOnce("dataReceived", function (oResponse) {
                        var aContexts = oResponse.getSource().getCurrentContexts();
                        fnResolve(aContexts);
                    });
                });
            }

            // Calculate total hours as far as the data is available
            Promise.all([oSpecialDatesPromise])
                .then(function (aContexts) {
                    if (aContexts.length > 0) {

                        var fTotalHours = 0.00,
                            fTargetHours = 0.00,
                            bEditable = false;

                        // Loop over all days
                        jQuery.each(aContexts[0], function (iIndex, oContext) {
                            var oData = oModel.getProperty(oContext.getPath()),
                                oDate = Formatter.formatAsUtcDate(oData.CaleDate);

                            // Process all days that are in the selected interval of days
                            if (oDate >= oFormattedStartDate && oDate <= oFormattedEndDate) {
                                // User selected a single day
                                if (oFormattedStartDate.valueOf() === oFormattedEndDate.valueOf()) {
                                    // Calculate assignable hours for the selected interval
                                    fTargetHours += parseFloat(oData.TargetHoursSingle);

                                    // Calculate total hours for the selected interval
                                    fTotalHours += parseFloat(oData.TotalHoursSingle);
                                } else {
                                    // Calculate assignable hours for the selected interval
                                    fTargetHours += parseFloat(oData.TargetHours);

                                    // Calculate total hours for the selected interval
                                    fTotalHours += parseFloat(oData.TotalHours);
                                }

                                bEditable = (oData.Editable) ? true : bEditable;

                            }

                        });

                        // Format total hours, using 2 decimals
                        fTargetHours = parseFloat(fTargetHours.toFixed(2));
                        fTotalHours = parseFloat(fTotalHours.toFixed(2));

                        // Set total hours as maximal constraint for assigned hours
                        var oInput = this.byId("frgTimeRecordingForm--inpAssignedHours");
                        oInput.getBinding("value").getType().oConstraints.maximum = fTargetHours;

                        // Assign calculated assignable hours
                        oRecordModel.setProperty("/HoursRemaining", fTargetHours);

                        // Assign total available hours
                        oRecordModel.setProperty("/TotalHours", fTotalHours);

                        // Activate editing if one of the days is editable
                        this.getModel("view").setProperty("/editable", bEditable);

                        // Set default values from local models (preassigned as from user settings) - only if the selected range is editable and time management mode is not active
                        if (bEditable && fTargetHours > 0 && !this.getModel("view").getProperty("/timeManagement")) {

                            // Wait for SettingsModel
                            var oSettingsModelPromise;

                            if (this.getModel("settings")) {
                                oSettingsModelPromise = this.getModel("settings");
                            } else {
                                oSettingsModelPromise = new Promise(function (fnResolve) {
                                    this.bus.subscribe("waitFor", "settingsModel", function (sChannel, sEvent, oData) {
                                        fnResolve(oData);
                                    });
                                }.bind(this));
                            }

                            // Continue when settings model is available
                            Promise.all([oSettingsModelPromise])
                                .then(function (aData) {

                                    var oSettingsModel = aData[0],
                                        oLoanCostCenterModel = this.getModel("loanCostCenters"),
                                        oFavoritesModel = this.getModel("favorites"),
                                        bUseLoanCostCenter = oSettingsModel.getProperty("/UseLoanCostCenter"),
                                        bUsePreAssignmentOfLoanCostCenter = oSettingsModel.getProperty("/UsePreAssignmentAct"),
                                        bUsePreAssignmentOfFavorite = oSettingsModel.getProperty("/UsePreAssignment"),
                                        bPreAssignedLoanCostCenterFound = false,
                                        bPreAssignedFavoriteFound = false;

                                    if (bUseLoanCostCenter && bUsePreAssignmentOfLoanCostCenter) {
                                        oLoanCostCenterModel.getData().forEach(function (oItem) {
                                            if (oItem.IsPreAssigned) {
                                                this.getModel("recording").setProperty("/LoanCostCenter", oItem.CostObjectID);
                                                bPreAssignedLoanCostCenterFound = true;
                                                return;
                                            }
                                        }.bind(this));
                                    }
                                    // Reset preassigned item, in case no item is selected as preassigned or it's not needed
                                    if (!bPreAssignedLoanCostCenterFound) {
                                        this.getModel("recording").setProperty("/LoanCostCenter", "");
                                    }

                                    if (bUsePreAssignmentOfFavorite) {
                                        oFavoritesModel.getData().forEach(function (oItem) {
                                            if (oItem.IsPreAssigned) {
                                                this.getModel("recording").setProperty("/CostObjectID", oItem.CostObjectID);
                                                bPreAssignedFavoriteFound = true;
                                                return;
                                            }
                                        }.bind(this));
                                    }
                                    // Reset preassigned item, in case no item is selected as preassigned or it's not needed
                                    if (!bPreAssignedFavoriteFound) {
                                        this.getModel("recording").setProperty("/CostObjectID", "");
                                    }

                                    oForm.setBusy(false);

                                }.bind(this));
                        } else {
                            oForm.setBusy(false);
                        }

                    }
                }.bind(this));

        },

        /**
         * Resets the selection of the calendar
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _resetCalendarSelection: function () {

            // The removing of the selected date has to be done after the binding of the calender settings happened
            var oCalendar = this.byId("calRecordingCalendar");
            if (oCalendar.getBindingContext()) {
                oCalendar.removeAllSelectedDates();
            } else if (oCalendarPromise) {
                oCalendarPromise.then(function (oData) {
                    oCalendar.removeAllSelectedDates();
                });
            }

            //oCalendar.removeAllSelectedDates();

            // Set default selection mode from settings
            var oView = this.getView(),
                oViewModel = oView.getModel("view"),
                oSettingsModel = oView.getModel("settings");

            // Get the user settings and apply the default selection method
            if (oSettingsModel) {
                oViewModel.setProperty("/selectionMode", (oSettingsModel.getProperty("/SelectionMethod") === "I") ? "interval" : "single");
            }
        },

        /**
         * Resets the fields of the form
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _resetForm: function () {

            var oRecordModel = this.getModel("recording");
            oRecordModel.setProperty("/CaleSelDateBegin", null);
            oRecordModel.setProperty("/CaleSelDateEnd", null);
            oRecordModel.setProperty("/AssignedHours", 0.00);
            oRecordModel.setProperty("/Percentage", 0.00);
            oRecordModel.setProperty("/CostObjectID", "");
            oRecordModel.setProperty("/CostObjectType", "");
            oRecordModel.setProperty("/HoursRemaining", 0.00);
            oRecordModel.setProperty("/TotalHours", 0.00);
            oRecordModel.setProperty("/LoanCostCenter", "");
        },

        /**
         * Adds disabled days to the calendar control depending on the borders of allowed selection and navigation
         * @param {Object} [oCalendarSettings] Parameters for the calendar control
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _addDisabledDays: function (oCalendarSettings) {

            // Format dates as UTC dates to avoid time zone errors
            var oCaleNavDateMin = Formatter.formatAsZeroHourUtcDate(oCalendarSettings.CaleNavDateMin),
                oCaleNavDateMax = Formatter.formatAsZeroHourUtcDate(oCalendarSettings.CaleNavDateMax),
                oCaleSelDateMin = Formatter.formatAsZeroHourUtcDate(oCalendarSettings.CaleSelDateMin),
                oCaleSelDateMax = Formatter.formatAsZeroHourUtcDate(oCalendarSettings.CaleSelDateMax);

            var oCalendar = this.byId("calRecordingCalendar"),
                oCurrentDate = oCaleNavDateMin;

            // Disable first area of days (from start of navigation to start of selection)
            for (oCurrentDate; oCurrentDate < oCaleSelDateMin; oCurrentDate.setDate(oCurrentDate.getDate() + 1)) {
                oCalendar.addDisabledDate(new DateRange({
                    startDate: new Date(oCurrentDate)
                }));
            }

            // Disable second area of days (from end of selection to end of navigation)
            oCurrentDate.setDate(oCaleSelDateMax.getDate() + 1);
            for (oCurrentDate; oCurrentDate <= oCaleNavDateMax; oCurrentDate.setDate(oCurrentDate.getDate() + 1)) {
                oCalendar.addDisabledDate(new DateRange({
                    startDate: new Date(oCurrentDate)
                }));
            }

        },

        /**
         * Channel event handler for event "selectCategory" on KPI overview
         * @param {string} [sChannel] The channel of the event to fire
         * @param {string} [sEvent] The identifier of the event to fire
         * @param {Object} [oData] The parameters which should be carried by the event
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _refreshAfterTimeRecordingChange: function (sChannel, sEvent, oData) {

            // Refresh binding of the special dates of the calendar
            var oCalendar = this.byId("calRecordingCalendar"),
                oSpecialDatesBinding = oCalendar.getBinding("specialDates");

            // Re-calculate form
            oSpecialDatesBinding.attachEventOnce("change", function () {
                this._buildFormForSelectedDates(this.oStartDate, this.oEndDate);
            }.bind(this));

            oSpecialDatesBinding.refresh(true);
        },

        /**
         * Resets the states of all validated fields
         * @memberOf porsche.cats.timerecording.Master
         * @private
         */
        _resetValidation: function () {

            // Remove messages from message manager
            var oMessageManager = sap.ui.getCore().getMessageManager();
            oMessageManager.removeAllMessages();

            // Reset value state of elements
            jQuery.each(aInputsToValidate, function (iIndex, sInputId) {
                var oInput = this.byId(sInputId);
                if (oInput) {
                    oInput.setValueState("None");
                }
            }.bind(this));
        }

    });

    return oMaster;

});
