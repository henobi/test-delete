/**
 * Created by mdeppe on 25.10.2019.
 */
sap.ui.define([
    "porsche/cats/timerecording/controller/BaseController"

], function (BaseController) {  // eslint-disable-line id-match
    "use strict";

    // noinspection UnnecessaryLocalVariableJS
    /**
     * NotFound.controller.js
     *
     * This page is shown if a target was not matched
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {object} [mSettings] initial settings for the new control
     *
     * @class NotFound.controller.js
     *
     * @extends porsche.cats.timerecording.controller.BaseController
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.NotFound
     */
    var oNotFound = BaseController.extend("porsche.cats.timerecording.controller.NotFound", {

        /* =========================================================== */
        /* Standard functions 										   */
        /* =========================================================== */

        /**
         * This function is called every time the view is loaded
         * @memberOf porsche.cats.timerecording.NotFound
         */
        onInit: function () {
            this.component = this.getOwnerComponent();
            this.bus = this.component.getEventBus();
        },

//      onBeforeRendering: function() {},

//      onAfterRendering: function() {},

//      onExit: function() {},

        /* =========================================================== */
        /* Utility functions of controller  						   */
        /* =========================================================== */

        /* =========================================================== */
        /* Event handlers (starts with "on")						   */
        /* =========================================================== */

        /**
         * Navigates to the top home view
         * @memberOf porsche.cats.timerecording.NotFound
         */
        onHomePressed: function () {

            // Check for pending changes before navigating
            this.getRouter().navTo("home");

        }

        /* =========================================================== */
        /* Private functions (starts with "_")						   */
        /* =========================================================== */

    });

    return oNotFound;

});
