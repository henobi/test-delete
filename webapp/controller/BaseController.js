/**
 * Created by mdeppe on 25.10.2019.
 */
sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "porsche/cats/timerecording/util/Formatter",
    "porsche/cats/timerecording/util/MessageContainer",
    "porsche/cats/timerecording/util/Models",
    "sap/m/Button",
    "sap/m/Dialog",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/ui/core/Icon",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/message/Message",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/m/Popover",
    "sap/m/Title",
    "sap/m/Text",
    "sap/m/Toolbar",
    "sap/m/ToolbarSpacer"

], function (Controller, UIComponent, Formatter, MessageContainer, Models, Button, Dialog, Filter, Fragment, Icon,          // eslint-disable-line id-match, max-params
             JSONModel, Message, MessageBox, MessageToast, Popover, Title, Text, Toolbar, ToolbarSpacer) {                  // eslint-disable-line id-match, max-params
    "use strict";

    var iMaxLoanCostCenters = 30;            // Limit of loan cost centers that can be added

    // noinspection UnnecessaryLocalVariableJS
    /**
     * BaseController.js
     *
     * This is the BaseController of the application, which contains some standard methods
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {Object} [mSettings] initial settings for the new control
     *
     * @class BaseController.js
     *
     * @extends sap.ui.core.mvc.Controller
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.BaseController
     */
    var oBaseController = Controller.extend("porsche.cats.timerecording.controller.BaseController", {

        Formatter: Formatter,

        /* =========================================================== */
        /* Standard functions 										   */
        /* =========================================================== */

        /**
         * Constructor function to declare some static variables
         * @memberOf porsche.cats.timerecording.BaseController
         */
        constructor: function () {
            var sRootPath = "porsche.cats.timerecording";
            this._sViewPath = sRootPath + ".view.";
            this._sDialogPath = sRootPath + ".view.dialog.";
            this._sFragmentPath = sRootPath + ".view.fragment.";

        },

        /* =========================================================== */
        /* Getter functions of controller  						   */
        /* =========================================================== */

        /**
         * Convenience method for accessing the router.
         * @returns {sap.ui.core.routing.Router} the router for this component
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getRouter: function () {
            return UIComponent.getRouterFor(this);
        },

        /**
         * Convenience method for getting the view model by name.
         * @param {string} [sName] the model name
         * @returns {sap.ui.model.Model} the model instance
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getModel: function (sName) {
            return this.getView().getModel(sName);
        },

        /**
         * Convenience method for setting the view model.
         * @param {sap.ui.model.Model} [oModel] The model instance
         * @param {string} [sName] The model name
         * @returns {sap.ui.mvc.View} The view instance
         * @memberOf porsche.cats.timerecording.BaseController
         */
        setModel: function (oModel, sName) {
            return this.getView().setModel(oModel, sName);
        },

        /**
         * Getter for the resource bundle.
         * @returns {sap.ui.model.resource.ResourceModel} The resourceModel of the component
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getResourceBundle: function () {
            return this.getOwnerComponent().getModel("i18n").getResourceBundle();
        },

        /**
         * Getter for view path
         * @returns {string} Path of view files
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getViewPath: function () {
            return this._sViewPath;
        },

        /**
         * Getter for dialog path
         * @returns {string} Path of dialog files
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getDialogPath: function () {
            return this._sDialogPath;
        },

        /**
         * Getter for fragments path
         * @returns {string} Path of fragment files
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getFragmentPath: function () {
            return this._sFragmentPath;
        },

        /**
         * Getter for maximum of loan cost center counter
         * @returns {integer} Maximal loan cost centers
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getMaxLoanCostCenters: function () {
            return iMaxLoanCostCenters;
        },

        /**
         * Returns a promise for receiving the asynchronous general info model
         * @returns {Promise<unknown[]>} The promise which is either already finished or not
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getGeneralInfoModelPromise: function () {

            var oGeneralInfoPromise;

            if (this.getModel("generalInfo")) {
                oGeneralInfoPromise = this.getModel("generalInfo");
            } else {
                oGeneralInfoPromise = new Promise(function (fnResolve) {
                    this.bus.subscribe("waitFor", "generalInfo", function (sChannel, sEvent, oData) {
                        fnResolve(oData);
                    });
                }.bind(this));
            }

            return Promise.all([oGeneralInfoPromise]);

        },

        /* =========================================================== */
        /* Setter functions of controller  						   */
        /* =========================================================== */

        /* =========================================================== */
        /* Utility functions of controller  						   */
        /* =========================================================== */

        /**
         * Shows an error message if a error occurs on the backend system
         * @memberOf porsche.cats.timerecording.BaseController
         */
        showServiceError: function () {
            MessageBox.error(this.getModel("i18n").getResourceBundle().getText("errorFromService"), {
                styleClass: this.getOwnerComponent().getContentDensityClass()
            });
        },

        /**
         * Shows the message with the highest priority and clears the stack
         * @memberOf porsche.cats.timerecording.BaseController
         */
        showHighestMessage: function () {
            MessageContainer.getInstance().showHighestMessage();
        },

        /**
         * Adds a message to the message container stack
         * @param {string} [sLangKey] The i18n key from the properties file
         * @param {number} [iPriority] The priority of the message (only highest will be shown)
         * @param {string} [sState] The state of the message
         * @memberOf porsche.cats.timerecording.BaseController
         */
        addI18nMessage: function (sLangKey, iPriority, sState) {
            // add error object for specified i18n text
            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            MessageContainer.getInstance().addMessage(oBundle.getText(sLangKey), iPriority, sState);

        },

        /**
         * Adds a message to the message container stack
         * @param {string} [sMessage] The message from service
         * @param {number} [iPriority] The priority of the message (only highest will be shown)
         * @param {string} [sState] The state of the message
         * @memberOf porsche.cats.timerecording.BaseController
         */
        addServiceMessage: function (sMessage, iPriority, sState) {
            var oServiceMessage = JSON.parse(sMessage);
            MessageContainer.getInstance().addMessage(oServiceMessage.error.message.value, iPriority, sState);
        },

        /**
         * Adds a message to the message container stack
         * @param {string} [sMessage] The message from service
         * @param {number} [iPriority] The priority of the message (only highest will be shown)
         * @param {string} [sState] The state of the message
         * @memberOf porsche.cats.timerecording.BaseController
         */
        addServerMessage: function (sMessage, iPriority, sState) {
            MessageContainer.getInstance().addMessage(sMessage, iPriority, sState);
        },

        /**
         * Returns the translated text for the provided i18n key
         * @param {string} [sTextKey] The i18n key for the requested text
         * @param {Array} [aVariables] A list of optional variables
         * @return {string} The translated text for the provided i18n key
         * @memberOf porsche.cats.timerecording.BaseController
         */
        getResourceText: function (sTextKey, aVariables) {
            if (this.getView().getModel("i18n")) { // check if the model is available
                return this.getView().getModel("i18n").getResourceBundle().getText(sTextKey, aVariables);
            } else {
                return "";
            }
        },

        /**
         * Saves the current changes on the model and calls the (optional) callbacks
         * @param {Object} [oSaveOptions] Object with necessary parameters and callbacks
         * @param {boolean} [oSaveOptions.isExplicitSave] Indicator if the save happens implicit
         * @param {boolean} [oSaveOptions.isCreate] Indicator if a create happens, requires a returning context
         * @param {Function} [oSaveOptions.success] Callback function in case of success
         * @param {Function} [oSaveOptions.error] Callback function in case of error
         * @memberOf porsche.cats.timerecording.BaseController
         */
        saveChanges: function (oSaveOptions) {

            var oModel = this.getView().getModel(),
                oSaveOptionsLocal = (oSaveOptions) ? oSaveOptions : {};

            // Check if all fields are valid
            if (this._isValid()) {

                // Remove messages from message manager
                var oMessageManager = sap.ui.getCore().getMessageManager();
                oMessageManager.removeAllMessages();

                // Check if there are pending changes
                if (oModel.hasPendingChanges()) {
                    oModel.submitChanges({

                        success: jQuery.proxy(function (oData) {

                            var sHttpCode,
                                oContext;

                            // Check http code
                            if (oData.__batchResponses[0].response) {

                                sHttpCode = oData.__batchResponses[0].response.statusCode;

                            } else if (oData.__batchResponses[0].__changeResponses[0]) {
                                sHttpCode = oData.__batchResponses[0].__changeResponses[0].statusCode;

                                // Check if the save happens in create mode and provide the created context to success handler
                                if (oSaveOptionsLocal.isCreate) {
                                    oContext = oData.__batchResponses[0].__changeResponses[0].data;
                                }
                            } else {
                                sHttpCode = "2";
                            }

                            if (sHttpCode.substr(0, 1) === "2") {

                                if (typeof oSaveOptionsLocal.success === "function") {
                                    // Execute the passed callback and submit the current context in create mode
                                    oSaveOptionsLocal.success(oContext);
                                }

                            } else {

                                if (typeof oSaveOptionsLocal.error === "function") { //eslint-disable-line no-lonely-if
                                    // Execute the passed callback
                                    oSaveOptionsLocal.error();
                                }
                            }
                        }),

                        error: jQuery.proxy(function (oEvent) {

                            this.addServerMessage(oEvent.statusText, 0, "Error");

                            if (typeof oSaveOptionsLocal.error === "function") {
                                // Execute the passed callback
                                oSaveOptionsLocal.error();
                            } else {
                                this.showHighestMessage();
                            }
                        }.bind(this))
                    });

                } else {
                    // In case there are no changes
                    if (oSaveOptionsLocal.isExplicitSave) {
                        this.addI18nMessage("noChanges");
                        this.showHighestMessage();
                    }
                    if (typeof oSaveOptionsLocal.success === "function") {
                        // Execute the passed callback
                        oSaveOptionsLocal.success();
                    } else {
                        this.showHighestMessage();
                    }
                }
            } else {
                this.addI18nMessage("invalidInputs", 0, "Error");
                if (typeof oSaveOptionsLocal.error === "function") {
                    // Execute the passed callback
                    oSaveOptionsLocal.error();
                } else {
                    this.showHighestMessage();
                }
            }

        },

        /**
         * Cancels all changes
         * @param {Object} [oCancelOptions] Object with necessary parameters and callbacks
         * @param {boolean} [oCancelOptions.showSecurityPopover] Defines if the security popover shall be shown
         * @param {sap.ui.core.Control} [oCancelOptions.popoverSourceControl] The source control to place the popover on
         * @param {sap.m.PlacementType} [oCancelOptions.popoverPlacement] Defines the placement of the popover
         * @param {Function} [oCancelOptions.success] Callback function in case of success
         * @memberOf porsche.cats.timerecording.BaseController
         */
        cancelChanges: function (oCancelOptions) {

            var oModel = this.getView().getModel();

            // Check if there are pending changes
            if (oModel.hasPendingChanges()) {

                if (oCancelOptions.showSecurityPopover) {
                    // Show security popover before canceling
                    this._continuePopoverYesNo(
                        function () {

                            // Remove messages from message manager
                            var oMessageManager = sap.ui.getCore().getMessageManager();
                            oMessageManager.removeAllMessages();

                            oModel.resetChanges();
                            if (typeof oCancelOptions.success === "function") {
                                oCancelOptions.success();
                            }
                        },
                        "questionCancelEdit",
                        oCancelOptions.popoverSourceControl,
                        oCancelOptions.popoverPlacement
                    );

                } else {
                    // Remove messages from message manager
                    var oMessageManager = sap.ui.getCore().getMessageManager();
                    oMessageManager.removeAllMessages();

                    // Cancel all changes
                    oModel.resetChanges();
                    if (typeof oCancelOptions.success === "function") {
                        oCancelOptions.success();
                    }
                }
            } else if (typeof oCancelOptions.success === "function") {
                // Go on, there are no changes
                oCancelOptions.success();
            }

        },

        /**
         * Deletes the entity submitted via path
         * @param {Object} [oDeleteOptions] Object with necessary parameters and callbacks
         * @param {string} [oDeleteOptions.path] The binding path of the entity to delete
         * @param {boolean} [oDeleteOptions.showSecurityPopover] Defines if the security popover shall be shown
         * @param {sap.ui.core.Control} [oDeleteOptions.popoverSourceControl] The source control to place the popover on
         * @param {sap.m.PlacementType} [oDeleteOptions.popoverPlacement] Defines the placement of the popover
         * @param {string} [oDeleteOptions.popoverQuestion] The question shown in the popover
         * @param {Function} [oDeleteOptions.success] Callback function in case of success
         * @param {Function} [oDeleteOptions.error] Callback function in case of error
         * @memberOf porsche.cats.timerecording.BaseController
         */
        deleteEntity: function (oDeleteOptions) {

            if (oDeleteOptions.showSecurityPopover) {
                // Show security popover before deleting
                this._continuePopoverYesNo(
                    function () {
                        this._deleteEntity(oDeleteOptions.path, oDeleteOptions.success, oDeleteOptions.error);
                    }.bind(this),
                    oDeleteOptions.popoverQuestion,
                    oDeleteOptions.popoverSourceControl,
                    oDeleteOptions.popoverPlacement,
                    true
                );

            } else {
                // Delete entity instantly
                this._deleteEntity(oDeleteOptions.path, oDeleteOptions.success, oDeleteOptions.error);
            }

        },

        /**
         * Checks for unsaved changes before navigating
         * @param {Object} [oNavigateOptions] Object with necessary parameters and callbacks
         * @param {Function} [oNavigateOptions.continue] Callback function in case of continue
         * @param {Function} [oNavigateOptions.stay] Callback function in case of error/cancel
         * @memberOf porsche.cats.timerecording.BaseController
         */
        checkSaveBeforeNavigate: function (oNavigateOptions) {

            var oModel = this.getView().getModel();

            // Check for unsaved changes
            if (oModel.hasPendingChanges()) {

                // Ask to save changes before continue
                this._continuePopupYesNo(
                    //In case of yes
                    function () {

                        // Save changes
                        this.saveChanges({
                            success: oNavigateOptions.continue,
                            error: oNavigateOptions.stay
                        });

                    }.bind(this),
                    // In case of no
                    function () {

                        // Remove messages from message manager
                        var oMessageManager = sap.ui.getCore().getMessageManager();
                        oMessageManager.removeAllMessages();

                        oModel.resetChanges();
                        if (typeof oNavigateOptions.continue === "function") {
                            oNavigateOptions.continue();
                        }

                    },
                    // In case of cancel
                    oNavigateOptions.stay,
                    "questionNavigateEdit"
                );

            } else if (typeof oNavigateOptions.continue === "function") {
                // In case of no changes, continue
                oNavigateOptions.continue();
            }

        },

        /**
         * Adds an message to the message manager
         * @param {Object} [oMessageOption] An object with message options
         * @param {string} [oMessageOption.message] The message text
         * @param {string} [oMessageOption.description] The message description
         * @param {string} [oMessageOption.additionalText] The message additionalText
         * @param {sap.ui.core.MessageType} [oMessageOption.type] The message type
         * @param {string} [oMessageOption.code] The message code
         * @param {string} [oMessageOption.target] The message target: The syntax is MessageProcessor dependent.
         * @param {boolean} [oMessageOption.technical] If the message is set as technical message
         * @memberOf porsche.cats.timerecording.BaseController
         */
        addMessageToMessageManager: function (oMessageOption) {

            // Add message to the message manager
            var oMessage = new Message({
                message: oMessageOption.message,
                description: oMessageOption.description,
                additionalText: oMessageOption.additionalText,
                type: oMessageOption.type,
                code: oMessageOption.code,
                target: oMessageOption.target,
                technical: oMessageOption.technical,
                processor: this.getView().getModel()
            });
            sap.ui.getCore().getMessageManager().addMessages(oMessage);
        },

        /**
         * Initialize the message popover fragment
         * @memberOf porsche.cats.timerecording.BaseController
         *
         */
        createMessagePopover: function () {

            var oView = this.getView();

            // Instantiate the message popover
            if (!this.byId("mpoMessagePopover")) {
                // Load asynchronous XML fragment
                Fragment.load({
                    id: oView.getId(),
                    controller: this,
                    name: this.getFragmentPath() + "MessagePopover"
                }).then(function (oMessagePopover) {
                    // Connect message popover to the view
                    oView.addDependent(oMessagePopover);
                    oMessagePopover.getBinding("items").attachChange(this.onMessageChange, this);
                }.bind(this));
            }

        },

        /**
         * Checks the input control for validity
         * @param {sap.m.Input} [oInput] Allows the user to enter and edit text or numeric values in one line.
         * @returns {boolean} Indicator for validity of the control
         * @memberOf porsche.cats.timerecording.BaseController
         */
        validateInput: function (oInput) {

            var oBinding = oInput.getBinding("value");
            if (oBinding) {
                var oType = oBinding.getType();
                if (oType) {

                    // Check default validator of control
                    try {
                        switch (oType.getName()) {
                            case "Float":
                            case "sap.ui.model.odata.type.Decimal":
                                // For Float the value has to be converted because of german "," as separator
                                var fValue = Formatter.formatDecimalAsFloatString(oType, oInput.getValue());
                                oType.validateValue(fValue);
                                break;
                            default:
                                oType.validateValue(oInput.getValue());
                                break;
                        }
                    } catch (oException) {
                        // Workaround for missing error message from control
                        if (oInput.getValueState() === "None") {
                            oInput.setValueStateText(oException.message);
                            oInput.setValueState("Error");
                            this.addMessageToMessageManager({
                                target: oInput.getId() + "/value",
                                message: oException.message,
                                type: "Error"
                            });
                        }
                        return false;
                    }

                    if (!this._additionalTypeCheck(oType, oInput)) {
                        return false;
                    }
                }
            }

            // Check for required field
            if (oInput.getRequired()) {
                if (!this._checkRequiredField(oInput)) {
                    return false;
                }
            }

            // If no validation breaks, the input must be correct
            oInput.setValueState("None");
            return true;
        },

        /**
         * Removes all messages from the message manager for the given target
         * @param {string} [sTarget] The message ID of the target
         * @memberOf porsche.cats.timerecording.BaseController
         */
        removeMessageForTarget: function (sTarget) {

            var oMessageManager = sap.ui.getCore().getMessageManager();

            jQuery.each(oMessageManager.getMessageModel().getData(), function (iIndex, oMessage) {
                if (sTarget === oMessage.target) {
                    oMessageManager.removeMessages(oMessage);
                }
            });
        },

        /**
         * Adds a decimal input mode to an input control, for showing a numpad at mobile devices
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.BaseController
         */
        addDecimalInputMode: function (oEvent) {
            // Get DOM Element of SAPUI5 control
            var oElement = oEvent.srcControl.$();
            // Find the input element inside the div and add input type to it
            oElement.find("input").attr("inputmode", "decimal");
        },

        /* =========================================================== */
        /* Event handlers (starts with "on")						   */
        /* =========================================================== */

        /**
         * Opens the message popover when the user clicks on the button
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onMessagePopoverPress: function (oEvent) {
            if (!this.byId("mpoMessagePopover")) {
                this.createMessagePopover();
            }

            this.byId("mpoMessagePopover").toggle(oEvent.getSource());
        },

        /**
         * Event handler for the message context change
         * Opens the message popover automatically if it's an error
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onMessageChange: function (oEvent) {

            var oButton = this.byId("btnMessagePopover"),
                oMessagePopover = this.byId("mpoMessagePopover"),
                aMessages = sap.ui.getCore().getMessageManager().getMessageModel().getData();

            // Check if current view is active
            if (this.getView().getParent().getCurrentPage() !== this.getView()) {
                return;
            }

            if (!oMessagePopover) {
                this.createMessagePopover();
                oMessagePopover = this.byId("mpoMessagePopover");
            }

            if (aMessages.length > 0) {

                var bOpenPopoverInstantly = false;

                // Analyse the types of the messages
                jQuery.each(aMessages, function (iIndex, oMessage) {

                    // Persist message in all cases, because sometimes there is automatically a second request fired,
                    // which will remove the messages again
                    oMessage.persistent = true;

                    // Dirty fix for language specific separator problem in SAPUI5 standard validation
                    if (oMessage.message.indexOf("Geben Sie eine Zahl") !== -1) {
                        oMessage.message = oMessage.message.replace(".", ",");
                    }

                    switch (oMessage.type) {
                        case sap.ui.core.MessageType.Error:
                            bOpenPopoverInstantly = true;
                            break;
                        case sap.ui.core.MessageType.Warning:
                            // Open technical message (= from backend) automatically
                            bOpenPopoverInstantly = true;
                            break;
                        case sap.ui.core.MessageType.Success:
                            MessageToast.show(oMessage.message);
                            // Remove message from message model after showing the message toast
                            sap.ui.getCore().getMessageManager().removeMessages(oMessage);
                            break;
                        case sap.ui.core.MessageType.Information:
                            MessageToast.show(oMessage.message);
                            // Remove message from message model after showing the message toast
                            sap.ui.getCore().getMessageManager().removeMessages(oMessage);
                            break;
                    }


                });

                // Open popover if it's an error or warning
                if (bOpenPopoverInstantly) {
                    // Using of timeout is not recommended, but used in official SAP documentation, too
                    setTimeout(function () {        // eslint-disable-line sap-timeout-usage
                        oMessagePopover.openBy(oButton);
                    }, 100);
                }
            }

        },

        /**
         * Opens thew dialog for the search help
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @param {string} sType - Indicates if this dialog should handle cost objects or loan cost centres
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onHandleValueHelp: function (oEvent, sType) {

            var oDialogData = {
                    isCostObject: sType === "costObject",
                    isLoanCostCenter: sType === "loanCostCenter"
                },
                oDialogModel = new JSONModel(oDialogData);

            var oView = this.getView();
            // Instantiate the sort dialog
            if (!this.byId("dlgSearchHelp")) {
                // load asynchronous XML fragment
                Fragment.load({
                    id: oView.getId(),
                    controller: this,
                    name: this.getDialogPath() + "CostObjectValueHelpDialog"
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    oView.addDependent(oDialog);
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.setModel(oDialogModel, "dialog");
                    oDialog.open();
                }.bind(this));
            } else {
                this.byId("dlgSearchHelp").setModel(oDialogModel, "dialog");
                this.byId("dlgSearchHelp").open();
            }
        },

        /**
         * Shows the favorites after opening the value help
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onAfterOpenValueHelp: function (oEvent) {

            var oDialog = oEvent.getSource(),
                bIsCostObject = oDialog.getModel("dialog").getProperty("/isCostObject"),
                bTimeManagement = this.getModel("view").getProperty("/timeManagement");
            // Only show favorites in case of no active time management mode and in case of cost object dialog
            if (bIsCostObject) {
                if (!bTimeManagement) {
                    // Don't show favorites in time management mode
                    this._bindFavoritesToValueHelp();
                }
                // Adjust title
                oDialog.setTitle(this.getResourceText("searchHelpDialogTitle"));
            } else {
                // Adjust title
                oDialog.setTitle(this.getResourceText("searchHelpDialogTitleLoanCostCenter"));
            }


        },

        /**
         * Shows the favorites when the user presses the button
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onShowFavoritesInValueHelp: function () {
            this._bindFavoritesToValueHelp();
        },

        /**
         * Closes the search help
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onCloseValueHelp: function () {
            var oSearchHelp = this.byId("dlgSearchHelp");
            if (oSearchHelp) {
                oSearchHelp.close();
            }
        },

        /**
         * Destroys the dialog after closing it
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onAfterCloseValueHelp: function (oEvent) {
            oEvent.getSource().destroy();
        },

        /**
         * Search for cost objects
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @param {string} [sDialogType] Indicates the type of the dialog, from which the function was called
         * @memberOf porsche.cats.timerecording.BaseController
         */
        onHandleSearch: function (oEvent, sDialogType) {

            var bIsLoanCostSearch;
            if (sDialogType === "costObject") {
                bIsLoanCostSearch = oEvent.getSource().getModel("dialog").getProperty("/isLoanCostCenter");
            }

            var sQuery = oEvent.getParameter("query"),
                iMinLength = 4;

            if (sQuery.length >= iMinLength) {
                var aFilters = [],
                    oTable = this.byId("tblCostObjects");

                // Depending on the type of the dialog, add filter for favorite or loan cost center
                if (sDialogType === "favorite") {
                    aFilters.push(new Filter({
                        path: "IsFavorite",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: false
                    }));
                } else if (sDialogType === "loanCostCenter") {
                    aFilters.push(new Filter({
                        path: "IsLoanCostCenter",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: false
                    }));
                } else if (sDialogType === "costObject") {
                    // If the dialog is the normal search help, include begin and end date from time range selection
                    aFilters.push(new Filter({
                        path: "CaleSelDateBegin",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.oStartDate
                    }));
                    aFilters.push(new Filter({
                        path: "CaleSelDateEnd",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.oEndDate
                    }));
                }

                // Add filter for query, depending on check box (description or id)
                var sQueryTarget = this.byId("rbDesc").getSelected() ? "CostObjectDesc" : "CostObjectID";
                aFilters.push(new Filter({
                    path: sQueryTarget,
                    operator: sap.ui.model.FilterOperator.Contains,
                    value1: sQuery
                }));

                // Add filter for the type of the account assigment abject
                if (sDialogType === "loanCostCenter" || bIsLoanCostSearch) {
                    aFilters.push(new Filter({
                        path: "CostObjectType",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: "KOSTL"
                    }));
                } else {
                    // eslint-disable-next-line no-nested-ternary
                    var sType = this.byId("rbAny").getSelected() ? "ALL" : (this.byId("rbJob").getSelected() ? "AUFTRAG" : "PSP");

                    aFilters.push(new Filter({
                        path: "CostObjectType",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: sType
                    }));
                }

                // Replace filters from table by new ones
                oTable.bindAggregation("items", {
                    path: "/SearchHelpSet",
                    template: this.byId("cliCostObject"),
                    filters: aFilters,
                    sorter: new sap.ui.model.Sorter({
                        path: "CostObjectType",
                        descending: false
                    }),
                    events: {
                        // eslint-disable-next-line require-jsdoc
                        dataReceived: function () {

                            // Only use tokenizer in case of settings dialog
                            if (sDialogType === "loanCostCenter" || sDialogType === "favorite") {
                                this._preselectSearchFromTokenizer(oTable);
                            }
                        }.bind(this)
                    }
                });

            } else {
                sap.m.MessageToast.show(this.getResourceText("enterMinimumCharacters", iMinLength));
            }
        },


        /**
         * This method opens a MessageBox that contains a hint.
         * @param {sap.ui.base.Event} [oEvent] An event object
         * @param {string} [sTranslationKeyHeadline] Key of the i18n translation headline
         * @param {string} [sTranslationKeyText] Key of the i18n translation text
         * @memberOf porsche.cats.timerecording.AccountAssignment
         */
        onOpenHint: function (oEvent, sTranslationKeyHeadline, sTranslationKeyText) {

            MessageBox.information(this.getResourceText(sTranslationKeyText), {
                title: this.getResourceText(sTranslationKeyHeadline),
                styleClass: this.getOwnerComponent().getContentDensityClass()
            });
        },

        /* =========================================================== */
        /* Private functions (starts with "_")						   */
        /* =========================================================== */

        /**
         * Is called when the tokenizer should be updated
         * @param {string} sTableId Table id
         * @param {int} iFreeSlots Number of available free slots
         * @param {string} sWarningMessage Warning message if no more slots available
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _initializeTokenizer: function (sTableId, iFreeSlots, sWarningMessage) {
            this.getView().setModel(Models.createTokenizerModel(), "tokenizer");
            this.getModel("tokenizer").setProperty("/tokens", []);
            this.getModel("tokenizer").setProperty("/tableId", sTableId);

            if (sWarningMessage && iFreeSlots) {
                this.getModel("tokenizer").setProperty("/applyLimit", true);
                this.getModel("tokenizer").setProperty("/freeSlots", iFreeSlots);
                this.getModel("tokenizer").setProperty("/limitReached", iFreeSlots < 1);
                this.getModel("tokenizer").setProperty("/warningMessage", sWarningMessage);
            }
        },

        /**
         * Add new selected items to the tokenizer (and check if they already exist)
         * @param {array} aItems Items that should be added to the tokenizer
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _appendToTokenizer: function (aItems) {
            var oTokenizerModel = this.getModel("tokenizer"),
                aTokenizerItems = oTokenizerModel.getProperty("/tokens"),
                iAvailableSlots = oTokenizerModel.getProperty("/freeSlots") - oTokenizerModel.getProperty("/tokens").length,
                bApplyLimit = oTokenizerModel.getProperty("/applyLimit");

            aItems.forEach(function (oItem) {
                var bExists = aTokenizerItems.find(function (oTokenizerItem) {
                    return oTokenizerItem.getBindingContext().getObject().CostObjectID === oItem.getBindingContext().getObject().CostObjectID;
                });

                if (!bExists) {
                    if (iAvailableSlots > 0 || !bApplyLimit) {
                        aTokenizerItems.push(oItem);
                        oItem.setProperty("selected", true);
                        iAvailableSlots--;
                    } else {
                        oItem.setProperty("selected", false);
                    }
                }
            });

            oTokenizerModel.setProperty("/tokens", aTokenizerItems);
            this._refreshTokenizer();
        },

        /**
         * Removes unselected items from the tokenizer
         * @param {array} aItems Items that should be added to the tokenizer
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _removeFromTokenizer: function (aItems) {
            var oTokenizerModel = this.getModel("tokenizer"),
                aTokenizerItems = oTokenizerModel.getProperty("/tokens");

            aTokenizerItems = aTokenizerItems.filter(function (oTokenizerItem) {
                return !aItems.find(function (oItem) {
                    return oTokenizerItem.getBindingContext().getObject().CostObjectID === oItem.getBindingContext().getObject().CostObjectID;
                });
            });

            oTokenizerModel.setProperty("/tokens", aTokenizerItems);
            this._refreshTokenizer();
        },

        /**
         * This function is called when the table data is loaded and the items should be pre selected from the tokenizer
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _preselectSearchFromTokenizer: function () {
            var oTokenizerModel = this.getModel("tokenizer"),
                oTable = this.byId(oTokenizerModel.getProperty("/tableId")),
                aTokenizerItems = oTokenizerModel ? oTokenizerModel.getProperty("/tokens") : null;

            if (!oTokenizerModel) {
                return;
            }

            oTable.getItems().forEach(function (oItem) {
                var bPreselected = aTokenizerItems.find(function (oTokenizerItem) {
                    return oTokenizerItem.getBindingContext().getObject().CostObjectID === oItem.getBindingContext().getObject().CostObjectID;
                });

                oItem.setProperty("selected", !!bPreselected);
            });

            this._refreshTokenizer();
        },

        /**
         * Is called when the tokenizer should be updated
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _refreshTokenizer: function () {
            var oTokenizerModel = this.getModel("tokenizer"),
                oTableDialog = this.byId(oTokenizerModel.getProperty("/tableId")),
                aTokens = oTokenizerModel.getProperty("/tokens").map(function (oElement) {
                    var oToken = new sap.m.Token({
                        text: oElement.getBindingContext().getObject().CostObjectDesc,
                        key: oElement.getBindingContext().getObject().CostObjectID,
                        editable: true
                    });

                    // eslint-disable-next-line no-shadow
                    oToken.attachDelete(function (oEvent) {
                        this._removeTokenFromTokenizer(oEvent.getSource());
                    }.bind(this));

                    return oToken;
                }.bind(this));

            oTokenizerModel.setProperty("/hasTokens", oTokenizerModel.getProperty("/tokens").length > 0);

            // Check, if more tokens can be added
            oTokenizerModel.setProperty("/limitReached", oTokenizerModel.getProperty("/freeSlots") <= oTokenizerModel.getProperty("/tokens").length);

            // Pass tokens to input field
            var oInput = this.byId("multiInputTokens");
            if (oInput) {
                oInput.setTokens(aTokens);
            }

            // Lock all checkboxes if limit reached, so no more elements can be added
            var bLimitReached = oTokenizerModel.getProperty("/applyLimit") && oTokenizerModel.getProperty("/limitReached");

            // Show limit message also as message toast
            if (bLimitReached) {
                sap.m.MessageToast.show(this.getResourceText("settingsLoanCostCenterMaximumReached", this.getMaxLoanCostCenters()));
            }

            oTableDialog.getItems().forEach(function (oItem) {
                sap.ui.getCore().byId(oItem.getId() + "-selectMulti").setProperty("enabled", !bLimitReached || oItem.getProperty("selected"));
            });
        },

        /**
         * Delete a key from the tokenizer
         * @param {Token} [oToken] Token
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _removeTokenFromTokenizer: function (oToken) {
            var sKey = oToken.getProperty("key"),
                oTokenizerModel = this.getModel("tokenizer"),
                oTable = this.byId(oTokenizerModel.getProperty("/tableId")),
                oItemDeselected = oTable.getItems().find(function (oItem) {
                    return oItem.getBindingContext().getObject().CostObjectID === sKey;
                }),
                aTokens = oTokenizerModel.getProperty("/tokens").filter(function (oElement) {
                    return sKey !== oElement.getBindingContext().getObject().CostObjectID;
                });

            oTokenizerModel.setProperty("/tokens", aTokens);

            if (oItemDeselected) {
                oItemDeselected.setProperty("selected", false);
            }

            this._refreshTokenizer();
        },

        /**
         * Calls the correct validation functions
         * @return {boolean} returns bool
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _isValid: function () {

            var bValid = true;

            if (typeof this.isValidView === "function") {
                bValid = this.isValidView();
            }

            return bValid;
        },

        /**
         * Displays a  a Popup to ask if the user wants to continue
         * Execute either the Continue callback functions or the Abort callback
         * @param {Function} fnYesCallBackFunction - callback functions called when button yes pressed
         * @param {Function} fnNoCallBackFunction - callback functions called when button no pressed
         * @param {Function} fnCancelCallBackFunction - callback functions called when button cancel pressed
         * @param {string} sResourceQuestion - identifier for i18n text
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _continuePopupYesNo: function (fnYesCallBackFunction, fnNoCallBackFunction, fnCancelCallBackFunction, sResourceQuestion) {

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var sMsg = oBundle.getText(sResourceQuestion);
            var oDialog = new Dialog({
                title: oBundle.getText("confirm"),
                type: "Message",
                content: new Text({
                    text: sMsg
                }),
                buttons: [new Button({
                    text: oBundle.getText("yes"),
                    press: function () {    // eslint-disable-line require-jsdoc
                        // Execute the passed callback function
                        // and close the popup afterwards
                        if (typeof fnYesCallBackFunction === "function") {
                            fnYesCallBackFunction();
                        }
                        oDialog.close();
                    }
                }),
                    new Button({
                        text: oBundle.getText("no"),
                        press: function () {    // eslint-disable-line require-jsdoc
                            if (typeof fnNoCallBackFunction === "function") {
                                // Execute the passed callback
                                fnNoCallBackFunction();
                            }
                            oDialog.close();
                        }
                    }),
                    new Button({
                        text: oBundle.getText("cancel"),
                        press: function () {    // eslint-disable-line require-jsdoc
                            if (typeof fnCancelCallBackFunction === "function") {
                                // Execute the passed callback
                                fnCancelCallBackFunction();
                            }
                            oDialog.close();
                        }
                    })

                ],
                afterClose: function () {   // eslint-disable-line require-jsdoc
                    oDialog.destroy();
                }
            });

            oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());

            oDialog.open();
        },

        /**
         * Displays a  a Popover to ask if the user wants to continue Only YES/NO as choices
         * Execute either the Continue callback functions or the Abort callback
         * @param {Function} fnYesCallBackFunction - callback functions called when buttonYes pressed
         * @param {string} sResourceQuestion - i18n text name
         * @param {sap.ui.core.Control} oControl - The source control for the Popover
         * @param {sap.m.PlacementType} [sPlacement] Defines the placement of the popover
         * @param {boolean} [bErrorStyle] Indicates if this popover shall be shown as error message
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _continuePopoverYesNo: function (fnYesCallBackFunction, sResourceQuestion, oControl, sPlacement, bErrorStyle) {

            var sPopoverPlacement = sPlacement;

            if (!sPopoverPlacement) {
                sPopoverPlacement = "Auto";
            }

            if (!sResourceQuestion) {
                sResourceQuestion = "questionDelete";   // eslint-disable-line no-param-reassign
            }

            var oBundle = this.getView().getModel("i18n").getResourceBundle();
            var sMsg = oBundle.getText(sResourceQuestion);
            var oPopover = new Popover({
                title: oBundle.getText("confirm"),
                placement: sPopoverPlacement,
                content: [new Text({
                    text: sMsg
                }).addStyleClass("sapUiSmallMargin")],
                footer: [new Toolbar({
                    content: [
                        new ToolbarSpacer(),
                        new Button({
                            text: oBundle.getText("yes"),
                            press: function () {    // eslint-disable-line require-jsdoc
                                // Execute the passed callback functions
                                // and close the popup afterwards
                                if (typeof fnYesCallBackFunction === "function") {
                                    fnYesCallBackFunction();
                                }
                                if (oPopover && oPopover.isOpen()) {
                                    oPopover.close();
                                }
                            }
                        }),
                        new Button({
                            text: oBundle.getText("no"),
                            press: function () {    // eslint-disable-line require-jsdoc
                                // Close popover
                                if (oPopover && oPopover.isOpen()) {
                                    oPopover.close();
                                }
                            }
                        })
                    ]
                })],
                afterClose: function () {   // eslint-disable-line require-jsdoc
                    oPopover.destroy();
                }
            });

            if (bErrorStyle) {
                oPopover.addStyleClass("popoverErrorStyle");
                oPopover.setCustomHeader(new Toolbar({
                    content: [
                        new ToolbarSpacer(),
                        new Icon({
                            src: "sap-icon://message-error"
                        }).addStyleClass("errorIcon"),
                        new Title({
                            text: oBundle.getText("confirm")
                        }),
                        new ToolbarSpacer()
                    ]
                }));
            }

            oPopover.openBy(oControl);
        },

        /**
         * Deletes the entity with the submitted path
         * @param {string} [sPath] The binding path of the entity to delete
         * @param {Function} [fnSuccess] Callback function in case of success
         * @param {Function} [fnError] Callback function in case of error
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _deleteEntity: function (sPath, fnSuccess, fnError) {

            var oModel = this.getView().getModel();

            oModel.refreshSecurityToken(function () {

                // Remove messages from message manager
                var oMessageManager = sap.ui.getCore().getMessageManager();
                oMessageManager.removeAllMessages();

                oModel.remove(sPath, {
                    success: function () {          // eslint-disable-line require-jsdoc
                        //this.addI18nMessage("deleteSuccess");

                        if (typeof fnSuccess === "function") {
                            // Execute the passed callback
                            fnSuccess();
                        }

                    },

                    error: function (oEvent) {            // eslint-disable-line require-jsdoc
                        if (oEvent.statusCode === 500) {
                            this.addServerMessage(oEvent.statusText, 0, "Error");
                        }

                        if (typeof fnError === "function") {
                            // Execute the passed callback
                            fnError();
                        } else {
                            this.showHighestMessage();
                        }
                    }.bind(this)
                });
            }.bind(this));
        },

        /**
         * Destroys the message popover after exiting a view
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _destroyMessagePopover: function () {

            var oMessagePopover = this.byId("mpoMessagePopover");

            if (oMessagePopover) {
                oMessagePopover.destroy();
            }
        },

        /**
         * Checks the input fields with type specific checks, because the default
         * validators from SAPUI5 are a little bit buggy
         * @param {sap.ui.model.type} [oType] The type of the control binding
         * @param {sap.m.Input} [oInput] Allows the user to enter and edit text or numeric values in one line
         * @returns {boolean} Indicator for validity of the control
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _additionalTypeCheck: function (oType, oInput) {

            // Check for correct type of value
            switch (oType.getName()) {
                case "Float":
                case "Currency":
                case "sap.ui.model.odata.type.Decimal":
                    if (!this._checkNumberField(oInput)) {
                        return false;
                    }
                    break;
                case "Integer":
                    if (!this._checkIntegerField(oInput)) {
                        return false;
                    }
                    break;
            }

            return true;
        },

        /**
         * Checks the required input control for a value
         * @param {sap.m.Input} [oInput] Allows the user to enter and edit text or numeric values in one line.
         * @returns {boolean} Indicator for validity of the control
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _checkRequiredField: function (oInput) {

            var sValue = oInput.getValue();

            if (oInput.getBinding) {
                var oType = oInput.getBinding("value").getType();
                if (oType && oType.getName() === "Float" || oType.getName() === "sap.ui.model.odata.type.Decimal") {
                    sValue = Formatter.formatDecimalAsFloatString(oType, sValue);

                }
            }

            if (!oInput.getValue() || parseFloat(sValue) === 0) {

                var sMessage = this.getResourceText("requiredInputError");
                oInput.setValueState("Error");
                oInput.setValueStateText(sMessage);

                this.addMessageToMessageManager({
                    target: oInput.getId() + "/value",
                    message: sMessage,
                    type: "Error"
                });

                return false;
            }

            return true;
        },

        /**
         * Checks if the value is a valid number
         * @param {sap.m.Input} [oInput] Allows the user to enter and edit text or numeric values in one line.
         * @returns {boolean} Indicator for validity of the control
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _checkNumberField: function (oInput) {

            var sValue = oInput.getValue();

            if (oInput.getBinding) {
                var oType = oInput.getBinding("value").getType();
                if (oType) {
                    sValue = Formatter.formatDecimalAsFloatString(oType, sValue);

                }
            }

            if (isNaN(sValue)) {

                var sMessage = this.getResourceText("notANumber");
                oInput.setValueState("Error");
                oInput.setValueStateText(sMessage);

                this.addMessageToMessageManager({
                    target: oInput.getId() + "/value",
                    message: sMessage,
                    type: "Error"
                });

                return false;
            }

            return true;
        },

        /**
         * Checks if the value is a valid integer
         * @param {sap.m.Input} [oInput] Allows the user to enter and edit text or numeric values in one line.
         * @returns {boolean} Indicator for validity of the control
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _checkIntegerField: function (oInput) {

            if (!Number.isInteger(oInput.getValue())) {

                var sMessage = this.getResourceText("notAnInteger");
                oInput.setValueState("Error");
                oInput.setValueStateText(sMessage);

                this.addMessageToMessageManager({
                    target: oInput.getId() + "/value",
                    message: sMessage,
                    type: "Error"
                });

                return false;
            }

            return true;
        },

        /**
         * For the first opening of the value help, the table should show the favorites of the user
         * @memberOf porsche.cats.timerecording.BaseController
         * @private
         */
        _bindFavoritesToValueHelp: function () {

            var oTable = this.byId("tblCostObjects");

            // Replace filters from table with filters for showing the favorites
            oTable.bindAggregation("items", {
                path: "/SearchHelpSet",
                template: this.byId("cliCostObject"),
                filters: [
                    new Filter({
                        path: "IsFavorite",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: true
                    }),
                    new Filter({
                        path: "CaleSelDateBegin",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.oStartDate
                    }),
                    new Filter({
                        path: "CaleSelDateEnd",
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: this.oEndDate
                    })
                ],
                sorter: new sap.ui.model.Sorter({
                    path: "SortOrder",
                    descending: false
                })
            });
        }

    });

    return oBaseController;

});
