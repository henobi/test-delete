/**
 * Created by mdeppe on 25.10.2019.
 */
sap.ui.define([
    "porsche/cats/timerecording/controller/BaseController",
    "porsche/cats/timerecording/util/Models",
    "sap/ui/model/json/JSONModel"

], function (BaseController, Models, JSONModel) {  // eslint-disable-line id-match
    "use strict";

    // noinspection UnnecessaryLocalVariableJS
    /**
     * App.controller.js
     *
     * Layout of the application
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {object} [mSettings] initial settings for the new control
     *
     * @class App.controller.js
     *
     * @extends porsche.cats.timerecording.controller.BaseController
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.App
     */
    var oApp = BaseController.extend("porsche.cats.timerecording.controller.App", {

        /* =========================================================== */
        /* Standard functions 										   */
        /* =========================================================== */

        /**
         * This function is called every time the view is loaded
         * @memberOf porsche.cats.timerecording.App
         */
        onInit: function () {
            var oView = this.getView();

            // Load general information
            this._loadGeneralInformation();

            this.component = this.getOwnerComponent();
            this.bus = this.component.getEventBus();

            oView.setModel(Models.createLayoutModel(), "layoutModel");

            // Initialize view model
            this.getView().setModel(Models.createViewModel(), "view");

            // Initialize JSON model for new time recording entries
            this.getView().setModel(Models.createTimeRecordingModelForFunctionImport(), "recording");

            // Attach model for controlling the responsiveness of tables
            this.getView().setModel(Models.createTableModel(), "table");

            // since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
            var iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
            this.getOwnerComponent().getModel().metadataLoaded().then(this._onMetadataSuccess.bind(this, iOriginalBusyDelay));
            this.getOwnerComponent().getModel().attachMetadataFailed(this._onMetadataFailed.bind(this, iOriginalBusyDelay));

            // apply content density mode to root view
            oView.addStyleClass(this.getOwnerComponent().getContentDensityClass());

            // Add event listener for reloading the setting models
            this.bus.subscribe("refreshData", "afterSettingsChange", this._loadLocalModels, this);
        },

//      onBeforeRendering: function() {},

//      onAfterRendering: function() {},

//      onExit: function() {},

        /* =========================================================== */
        /* Utility functions of controller  						   */
        /* =========================================================== */

        /* =========================================================== */
        /* Event handlers (starts with "on")						   */
        /* =========================================================== */
        /**
         * React to FlexibleColumnLayout resize events
         * Hides navigation buttons and switches the layout as needed
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.App
         */
        onStateChange: function (oEvent) {
            var sLayout = oEvent.getParameter("layout") + "",
                iColumns = parseInt(oEvent.getParameter("maxColumnsCount"), 10),
                bForcedLayout = this.getModel("layoutModel").getProperty("/forcedLayout");

            if (iColumns === 1) {
                this.getModel("layoutModel").setProperty("/smallScreenMode", true);
                this._setLayout("One");
            } else {
                this.getModel("layoutModel").setProperty("/smallScreenMode", false);
                // Switch back to two column mode when device orientation is changed
                if (sLayout === "OneColumn" && !bForcedLayout) {
                    this._setLayout("Two");
                }
            }
        },

        /* =========================================================== */
        /* Private functions (starts with "_")						   */
        /* =========================================================== */

        /**
         * Sets the flexible column layout to one, two, or three columns for the different scenarios across the app
         * @param {string} sColumns the target amount of columns
         * @memberOf porsche.cats.timerecording.App
         * @private
         */
        _setLayout: function (sColumns) {
            if (sColumns) {
                this.getModel("layoutModel").setProperty("/layout", sColumns + "Column" + (sColumns === "One" ? "" : "sMidExpanded"));
            }
        },

        /**
         * Gets called when the loading of metadata succeeded
         * @param {integer} [iDelay] The delay for busyIndicatorDelay of the FlexibleColumnLayout
         * @memberOf porsche.cats.timerecording.App
         * @private
         */
        _onMetadataSuccess: function (iDelay) {
            var oLayoutModel = this.getView().getModel("layoutModel");
            oLayoutModel.setProperty("/busy", false);
            oLayoutModel.setProperty("/delay", iDelay);
        },

        /**
         * Gets called when the loading of metadata failed
         * @param {integer} [iDelay] The delay for busyIndicatorDelay of the FlexibleColumnLayout
         * @memberOf porsche.cats.timerecording.App
         * @private
         */
        _onMetadataFailed: function (iDelay) {
            var oLayoutModel = this.getView().getModel("layoutModel");
            oLayoutModel.setProperty("/busy", false);
            oLayoutModel.setProperty("/delay", iDelay);

            // Unfortunately this does not work, a workaround is implemented in the component init
            // MessageBox.error(this.getResourceText("errorServiceUnavailable"));
        },

        /**
         * Reads the header information, containing the personal number
         * @memberOf porsche.cats.timerecording.App
         * @private
         */
        _loadGeneralInformation: function () {

            var oModel = this.getModel(),
                oView = this.getView();

            oView.setBusy(true);

            // Call function import to get general settings
            oModel.callFunction("/LoadGeneralInfo", {
                method: "GET",
                success: function (oResponse, oStatus) {

                    // Check if the status from backend is correct and results are available
                    if (oStatus.statusCode === "200" && typeof oResponse === "object") {

                        // Determine general information binding from first result of the EntitySet
                        var sGeneralSettingsPath = oModel.createKey("GeneralInfoSet", {
                            Pernr: oResponse.Pernr
                        });

                        oView.bindElement({
                            path: "/" + sGeneralSettingsPath
                        });

                        oView.setModel(new JSONModel(oResponse), "generalInfo");

                        this.bus.publish("waitFor", "generalInfo", oResponse);

                        // Load settings and favorites/loan cost centers into local models
                        this._loadLocalModels();

                    } else {
                        // Show error if status or response is not correct
                        this.showServiceError();
                    }
                }.bind(this),
                error: function () {
                    this.showServiceError();
                    oView.setBusy(false);
                }.bind(this)
            });
        },

        /**
         * Loads all local models that are needed for the form
         * @memberOf porsche.cats.timerecording.App
         * @private
         */
        _loadLocalModels: function () {

            var oModel = this.getModel(),
                oView = this.getView(),
                sSettingsPath = oModel.createKey("UserSettingSet", {
                    Pernr: this.getModel("generalInfo").getProperty("/Pernr")
                });

            // Read settings into local settings model
            oModel.read("/" + sSettingsPath, {
                urlParameters: {
                    $expand: "UserFavouriteSet,UserLoanCostCenterSet"
                },
                success: function (oResponse, oStatus) {        // eslint-disable-line require-jsdoc

                    if (oStatus.statusCode === "200") {

                        // Create extra data objects for user settings, favorites and loan cost center
                        var oSettingsData = oResponse,
                            oLoanCostCenterData = oSettingsData.UserLoanCostCenterSet.results,
                            oFavoriteData = oSettingsData.UserFavouriteSet.results;
                        delete oSettingsData.UserLoanCostCenterSet;
                        delete oSettingsData.UserFavouriteSet;

                        // Add default initial value for loan cost center (to make it possible to chose no center)
                        oLoanCostCenterData.splice(0, 0, {
                            Pernr: oResponse.Pernr,
                            CostObjectType: "",
                            CostObjectID: "",
                            CostObjectDesc: "",
                            IsPreAssigned: false,
                            SortOrder: -1,
                            emptyValue: true
                        });

                        // Assign models to view
                        var oSettingsModel = new JSONModel(oSettingsData),
                            oFavoriteModel = new JSONModel(oFavoriteData),
                            oLoanCostCenterModel = new JSONModel(oLoanCostCenterData);
                        oView.setModel(oSettingsModel, "settings");
                        oView.setModel(oFavoriteModel, "favorites");
                        oView.setModel(oLoanCostCenterModel, "loanCostCenters");

                        // Check if correct calender selection mode is selected
                        this.bus.publish("waitFor", "settingsModel", oSettingsModel);

                    }

                    oView.setBusy(false);

                }.bind(this),
                error: function () {            // eslint-disable-line require-jsdoc
                    oView.setBusy(false);
                }
            });
        }
    });

    return oApp;

});
