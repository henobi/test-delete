/**
 * Created by mdeppe on 14.01.2020.
 */
sap.ui.define([
    "porsche/cats/timerecording/controller/BaseController",
    "porsche/cats/timerecording/util/Models",
    "sap/ui/model/Filter",
    "sap/ui/core/Fragment",
    "sap/m/MessageBox"

], function (BaseController, Models, Filter, Fragment, MessageBox) {  // eslint-disable-line id-match
    "use strict";

    // noinspection UnnecessaryLocalVariableJS
    /**
     * UserSettings.controller.js
     *
     * This view contains some user settings
     *
     * @param {string} [sId] id for the new control, generated automatically if no id is given
     * @param {object} [mSettings] initial settings for the new control
     *
     * @class UserSettings.controller.js
     *
     * @extends porsche.cats.timerecording.controller.BaseController
     *
     * @constructor
     * @public
     * @alias porsche.cats.timerecording.UserSettings
     */
    var oUserSettings = BaseController.extend("porsche.cats.timerecording.controller.UserSettings", {

        /* =========================================================== */
        /* Standard functions 										   */
        /* =========================================================== */

        /**
         * This function is called every time the view is loaded
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onInit: function () {
            this.component = this.getOwnerComponent();
            this.bus = this.component.getEventBus();

            // Initialize message model
            var oMessageManager = sap.ui.getCore().getMessageManager();
            this.getView().setModel(oMessageManager.getMessageModel(), "message");

            // Activate automatic message generation for complete view
            oMessageManager.registerObject(this.getView(), true);

            // Create message popover
            this.createMessagePopover();

            // Initialize the router
            var oRouter = this.getRouter();
            oRouter.getRoute("userSettings").attachMatched(
                this._onRouteMatched, this);

            this.getView().bindElement({
                path: "UserSetting"
            });

            // Handle resize events of table for responsive behavior
            var oLoanCostCenterTable = this.byId("tblLoanCostCenters");
            oLoanCostCenterTable.addEventDelegate({
                onAfterRendering: this.onTableResized.bind(this, "clmLoanCostCenterActionButtons", "actionButtonsVisibleOnLoanCostCenterTable")
            });
            var oFavoriteTable = this.byId("tblFavorites");
            oFavoriteTable.addEventDelegate({
                onAfterRendering: this.onTableResized.bind(this, "clmFavoriteActionButtons", "actionButtonsVisibleOnFavoriteTable")
            });

        },

//      onBeforeRendering: function() {},

//      onAfterRendering: function() {},

//      onExit: function() {},

        /* =========================================================== */
        /* Utility functions of controller  						   */
        /* =========================================================== */

        /* =========================================================== */
        /* Event handlers (starts with "on")						   */
        /* =========================================================== */

        /**
         * This method is called every time the table do's a re-rendering (f.e. because of resizing the view)
         * @param {string} [sId] The ID of the column, which contains the action buttons
         * @param {string} [sPropertyName] The name of the property in the tables model
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onTableResized: function (sId, sPropertyName, oEvent) {

            var oColumn = this.byId(sId);

            if (oColumn) {
                var bActionButtonsVisible = oColumn._media.matches;

                this.getModel("table").setProperty("/" + sPropertyName, bActionButtonsVisible);
            }
        },

        /**
         * Navigates back to home view
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onNavToHome: function () {
            // Check for pending changes before navigation
            this.checkSaveBeforeNavigate({
                // In case of positive feedback
                "continue": function () {
                    this.getRouter().navTo("home");
                    this.bus.publish("refreshData", "afterSettingsChange");
                }.bind(this)
            });
        },

        /**
         * Save all changes of the user settings view
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onSaveUserSettings: function () {

            var oView = this.getView();

            oView.setBusy(true);

            // Save all changes
            this.saveChanges({
                isExplicitSave: true,
                success: function (oContext) {      // eslint-disable-line require-jsdoc

                    this.bus.publish("refreshData", "afterSettingsChange");

                    oView.setBusy(false);

                }.bind(this),
                error: function () {            // eslint-disable-line require-jsdoc
                    oView.setBusy(false);
                },
                isCreate: false
            });

        },

        /**
         * Cancel all changes and navigates back to home view
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onCancelUserSettings: function (oEvent) {
            // Cancel all changes and navigate to home
            this.cancelChanges({
                success: function () {
                    // Navigate back to home view
                    this.getRouter().navTo("home");
                }.bind(this),
                showSecurityPopover: true,
                popoverSourceControl: oEvent.getSource(),
                popoverPlacement: "Top"
            });
        },

        /**
         * This method is triggered every time a loan cost center or favorite is selected / deselected from the search dialog.
         * The goal of this method is to limit the selection to the maximum of X loan cost centers in total.
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onChangeSearchDialogSelection: function (oEvent) {
            var oTableDialog = oEvent.getSource();

            // Remove unselected elements
            this._removeFromTokenizer(oTableDialog.getItems().filter(function (oItem) {
                return !oItem.getProperty("selected");
            }));

            // Merge new elements with the tokenizer
            this._appendToTokenizer(oTableDialog.getItems().filter(function (oItem) {
                return oItem.getProperty("selected");
            }));
        },


        /**
         * Open the dialog to create a new loan cost center
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onOpenAddingLoanCostCenterDialog: function () {
            var oView = this.getView(),
                oTable = this.byId("tblLoanCostCenters"),
                iCurrentCostCenters = oTable.getItems().length,
                sWarningMessage = this.getResourceText("settingsLoanCostCenterMaximumReached", this.getMaxLoanCostCenters())
            ;

            // Number of maximum loan cost centers already reached
            if (iCurrentCostCenters >= this.getMaxLoanCostCenters()) {
                MessageBox.warning(sWarningMessage, {
                    styleClass: this.getOwnerComponent().getContentDensityClass()
                });

                return;
            }

            // Instantiate the dialog for adding a loan cost center
            if (!this.byId("dlgAddLoanCostCenter")) {

                // Calculate free slots (Number of object that can still be added)
                var iFreeSlots = this.getMaxLoanCostCenters() - oTable.getItems().length;

                // Initialize tokenizer
                this._initializeTokenizer("tblCostObjects", iFreeSlots, sWarningMessage);

                // Load fragment
                Fragment.load({
                    id: oView.getId(),
                    controller: this,
                    name: this.getDialogPath() + "AddLoanCostCenterDialog"
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    oView.addDependent(oDialog);
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.open();
                }.bind(this));

            } else {
                this.byId("dlgAddLoanCostCenter").open();
            }
        },

        /**
         * Remove a loan cost object
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @param {boolean} [bToolbar] Specifies if an object was deleted via the toolbar in responsive mobile mode
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onRemoveLoanCostObject: function (oEvent, bToolbar) {
            var oContext,
                oTable = this.byId("tblLoanCostCenters");

            if (bToolbar) {
                oContext = oTable.getSelectedContexts()[0];

                if (!oContext) {
                    sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
                    return;
                }

            } else {
                oContext = oEvent.getSource().getParent().getBindingContext();
            }

            this.deleteEntity({
                path: oContext.getPath(),
                showSecurityPopover: true,
                popoverSourceControl: oEvent.getSource(),
                popoverPlacement: "PreferredBottomOrFlip",
                popoverQuestion: "questionDelete",
                success: function () {          // eslint-disable-line require-jsdoc
                    oTable.getBinding("items").refresh(true);
                }
            });
        },

        /**
         * Adds all selected loan cost centers
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onAddLoanCostCenters: function (oEvent) {
            var oTokenizerModel = this.getModel("tokenizer"),
                aTokenizerItems = oTokenizerModel.getProperty("/tokens"),
                oModel = this.getModel(),
                oDialog = oEvent.getSource().getParent();

            if (aTokenizerItems.length > 0) {
                var aPromises = [];

                // Create loan cost object entries for all selected cost objects
                jQuery.each(aTokenizerItems, function (iIndex, oItem) {
                    var oContext = oItem.getBindingContext();
                    aPromises.push(new Promise(
                        function (fnResolve, fnReject) {
                            oModel.create("/UserLoanCostCenterSet", {
                                Pernr: oContext.getProperty("Pernr"),
                                CostObjectType: oContext.getProperty("CostObjectType"),
                                CostObjectID: oContext.getProperty("CostObjectID"),
                                CostObjectDesc: oContext.getProperty("CostObjectDesc")
                            }, {
                                success: fnResolve,
                                error: fnReject
                            });
                        }
                    ));
                });

                Promise.all(aPromises)
                    .then(function (oData, oResponse) {
                        oDialog.close();
                    })
                    .catch(function (oError) {
                        sap.m.MessageToast.show(this.getResourceText("settingsLoanCostCenterAddedFailed"));
                    }.bind(this));

            } else {
                sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
            }
        },

        /**
         * Remove a favourite
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @param {boolean} [bToolbar] Specifies if an object was deleted via the toolbar in responsive mobile mode
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onRemoveFavorite: function (oEvent, bToolbar) {
            var oContext,
                oTable = this.byId("tblFavorites");

            if (bToolbar) {
                oContext = oTable.getSelectedContexts()[0];

                if (!oContext) {
                    sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
                    return;
                }

            } else {
                oContext = oEvent.getSource().getParent().getBindingContext();
            }

            this.deleteEntity({
                path: oContext.getPath(),
                showSecurityPopover: true,
                popoverSourceControl: oEvent.getSource(),
                popoverPlacement: "PreferredBottomOrFlip",
                popoverQuestion: "questionDelete",
                success: function () {          // eslint-disable-line require-jsdoc
                    oTable.getBinding("items").refresh(true);
                }
            });
        },

        /**
         * Opens the dialog for adding favorites
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onOpenAddingFavoritesDialog: function () {
            var oView = this.getView();
            // Instantiate the dialog for adding favorites
            if (!this.byId("dlgFavoriteSelection")) {

                // Initialize tokenizer
                this._initializeTokenizer("tblCostObjects");

                // load asynchronous XML fragment
                Fragment.load({
                    id: oView.getId(),
                    controller: this,
                    name: this.getDialogPath() + "AddFavoritesDialog"
                }).then(function (oDialog) {
                    // connect dialog to the root view of this component (models, lifecycle)
                    oView.addDependent(oDialog);
                    oDialog.addStyleClass(this.getOwnerComponent().getContentDensityClass());
                    oDialog.open();
                }.bind(this));
            } else {
                this.byId("dlgFavoriteSelection").open();
            }
        },

        /**
         * Adds all selected favorites
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onAddFavorites: function (oEvent) {
            var oTokenizerModel = this.getModel("tokenizer"),
                aTokenizerItems = oTokenizerModel.getProperty("/tokens"),
                oModel = this.getModel(),
                oDialog = oEvent.getSource().getParent();

            if (aTokenizerItems.length > 0) {
                var aPromises = [];

                // Create favorites entries for all selected cost objects
                jQuery.each(aTokenizerItems, function (iIndex, oItem) {
                    var oContext = oItem.getBindingContext();
                    aPromises.push(new Promise(
                        function (fnResolve, fnReject) {
                            oModel.create("/UserFavouriteSet", {
                                Pernr: oContext.getProperty("Pernr"),
                                CostObjectType: oContext.getProperty("CostObjectType"),
                                CostObjectID: oContext.getProperty("CostObjectID"),
                                CostObjectDesc: oContext.getProperty("CostObjectDesc"),
                                IsFavourite: true
                            }, {
                                success: fnResolve,
                                error: fnReject
                            });
                        }
                    ));
                });

                Promise.all(aPromises)
                    .then(function (oData, oResponse) {
                        oDialog.close();
                    })
                    .catch(function (oError) {
                        sap.m.MessageToast.show(this.getResourceText("settingsFavoritesAddedFailed"));
                    }.bind(this));

            } else {
                sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
            }
        },

        /**
         * Closes the dialog
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onCloseDialog: function (oEvent) {
            oEvent.getSource().getParent().close();
        },

        /**
         * Destroys the dialog after closing it
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onAfterCloseDialog: function (oEvent) {
            oEvent.getSource().destroy();
        },

        /**
         * Changes the order of an item after dropping it on another position
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onDropItemOnNewPosition: function (oEvent) {

            var oDraggedItem = oEvent.getParameter("draggedControl"),
                oDroppedItem = oEvent.getParameter("droppedControl"),
                oTable = oDraggedItem.getParent(),
                iDragIndex = oTable.indexOfItem(oDraggedItem),
                iDropIndex = oTable.indexOfItem(oDroppedItem),
                iNewIndex = iDropIndex,
                sDropPosition = oEvent.getParameter("dropPosition"),
                oDraggedItemContext = oDraggedItem.getBindingContext();

            // Check for context
            if (!oDraggedItemContext) {
                return;
            }

            // Calculate the new rank for the dropped item depending on drop-position
            if (sDropPosition === "Before" && iDragIndex < iDropIndex) {
                iNewIndex -= 1;
            } else if (sDropPosition === "After" && iDragIndex > iDropIndex) {
                iNewIndex += 1;
            }

            this._reorderItemsOfTable(iDragIndex, iNewIndex, oTable);
        },

        /**
         * Moves the selected item of the list to one direction
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @param {string} [sDirection] The direction of the button (up/down)
         * @param {string} [sTableId] The ID of the List control
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onMoveSortOrderItem: function (oEvent, sDirection, sTableId) {

            var oTable = this.byId(sTableId),
                oSelectedItem = oTable.getSelectedItem();

            if (!oSelectedItem) {
                sap.m.MessageToast.show(this.getResourceText("selectAnEntry"));
                return;
            }

            var iIndexOfSelectedItem = oTable.indexOfItem(oSelectedItem);

            switch (sDirection) {
                case "up":
                    if (iIndexOfSelectedItem > 0) {
                        this._reorderItemsOfTable(iIndexOfSelectedItem, iIndexOfSelectedItem - 1, oTable);
                    }
                    break;
                case "down":
                    if (iIndexOfSelectedItem < oTable.getItems().length - 1) {
                        this._reorderItemsOfTable(iIndexOfSelectedItem, iIndexOfSelectedItem + 1, oTable);
                    }
                    break;
            }

        },

        /**
         * Block user input (e.g. tokenizer)
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onUserInputAvoid: function (oEvent) {
            oEvent.getSource().setValue("");
        },

        /**
         * Deselects the loan cost center pre-assignment, if the loan cost center was deactivated
         * @param {sap.ui.base.Event} [oEvent] An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         */
        onUserLoanCostCenterChanged: function (oEvent) {
            if (!oEvent.getParameter("selected")) {
                this.getModel().setProperty("UsePreAssignmentAct", false, this.getView().getBindingContext());
            }
        },

        /* =========================================================== */
        /* Private functions (starts with "_")						   */
        /* =========================================================== */

        /**
         * Is called every time the route was matched
         * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _onRouteMatched: function (oEvent) {
            // Show this view in full screen
            this.getModel("layoutModel").setProperty("/layout", "OneColumn");
            this.getModel("layoutModel").setProperty("/forcedLayout", true);

        },

        /**
         * This method reorders the items of a list depending of the moved item
         * @param {integer} [iSourceIndex] The index of the dragged item
         * @param {integer} [iTargetIndex] The index of the drop-position
         * @param {sap.m.Table} [oTable] The List control provides a container for all types of list items
         * @memberOf porsche.cats.timerecording.UserSettings
         * @private
         */
        _reorderItemsOfTable: function (iSourceIndex, iTargetIndex, oTable) {

            // A temp array for the positions is necessary, because the immediate update of the sort order will lead into wrong processing order
            var oItemContext,
                aNewOrder = [];

            // Calculate sort order of items before dropped or dragged item
            for (var iPreIndex = 0; iPreIndex < iSourceIndex && iPreIndex < iTargetIndex; iPreIndex++) {
                oItemContext = oTable.getItems()[iPreIndex].getBindingContext();

                if (oItemContext) {
                    // If context is available, save the new position of the item into temp array
                    aNewOrder.push({
                        context: oItemContext,
                        position: iPreIndex        // Position will be the same as before
                    });
                }
            }

            // Calculate sort order of items between the drag & drop positions, if dragged item was placed behind old position
            for (var iAfterDragIndex = iSourceIndex + 1; iAfterDragIndex <= iTargetIndex; iAfterDragIndex++) {
                oItemContext = oTable.getItems()[iAfterDragIndex].getBindingContext();

                if (oItemContext) {
                    // If context is available, save the new position of the item into temp array
                    aNewOrder.push({
                        context: oItemContext,
                        position: iAfterDragIndex - 1       // Move all items one place before
                    });
                }

            }

            // Calculate sort order of items between the drag & drop positions, if dragged item was placed before old position
            for (var iAfterDropIndex = iTargetIndex; iAfterDropIndex < iSourceIndex; iAfterDropIndex++) {
                oItemContext = oTable.getItems()[iAfterDropIndex].getBindingContext();

                if (oItemContext) {
                    // If context is available, save the new position of the item into temp array
                    aNewOrder.push({
                        context: oItemContext,
                        position: iAfterDropIndex + 1       // Move all items one place behind
                    });
                }
            }

            // Calculate sort order of the dragged item itself
            oItemContext = oTable.getItems()[iSourceIndex].getBindingContext();

            if (oItemContext) {
                // If context is available, save the new position of the item into temp array
                aNewOrder.push({
                    context: oItemContext,
                    position: iTargetIndex       // Move the dragged item to new position
                });
            }

            // Calculate the sort order of the items after the drag & drop positions
            for (var iPostIndex = (iTargetIndex > iSourceIndex) ? iTargetIndex + 1 : iSourceIndex + 1; iPostIndex < oTable.getItems().length; iPostIndex++) {
                oItemContext = oTable.getItems()[iPostIndex].getBindingContext();

                if (oItemContext) {
                    // If context is available, save the new position of the item into temp array
                    aNewOrder.push({
                        context: oItemContext,
                        position: iPostIndex       // Position will be the same as before
                    });
                }
            }

            var sPreassignedPath;

            // Remember path of preassigned item, because the flag will got lost when the sort order is changed
            jQuery.each(aNewOrder, function (iTemp, oOrderItem) {
                if (oOrderItem.context.getObject().IsPreAssigned) {
                    sPreassignedPath = oOrderItem.context.sPath;
                }
            });

            // Now order the items using the sort order Property
            jQuery.each(aNewOrder, function (iTemp, oOrderItem) {
                oTable.getModel().setProperty(oOrderItem.context.sPath + "/SortOrder", oOrderItem.position);
            });

            // Re-activate pre-assignment flag on correct item
            if (sPreassignedPath) {
                oTable.getModel().setProperty(sPreassignedPath + "/IsPreAssigned", true);
            }
        }
    });

    return oUserSettings;

});
