/**
 * Sync translation files
 * @author Hendrik Obermayer
 */
class SyncI18nTranslations
{
    constructor(dir) {
        this.fs = require('fs');
        this.dir = dir + (dir.endsWith("/") ? "" : "/");
        this.translationKeys = new Map;
    }

    _addTranslationKey(file, key) {
        if (!this.translationKeys.has(file)) {
            this.translationKeys.set(file, new Set);
        }

        this.translationKeys.get(file).add(key);
    }

    _getTranslationSet(file) {
        if (!this.translationKeys.has(file)) {
            return new Set;
        }

        return this.translationKeys.get(file);
    }

    _getTranslationFiles() {
        return this.fs.readdirSync(this.dir).map(file => this.dir + file);
    }

    _extendTranslationFile(filePath, missingKeys) {
        if (missingKeys.size === 0) {
            return;
        }

        const stream = this.fs.createWriteStream(filePath, {flags: 'a'});
        stream.write("\n\n\n# Missing translation keys");

        // Write missing keys to file
        missingKeys.forEach(key => stream.write("\n" + key +"=# TODO: missing translation"));

        // Close
        stream.write("\n");
        stream.close();
    }

    sync() {
        const files = this._getTranslationFiles();
        let missingTranslationKeysCount = 0;

        // Create set with all translation keys
        let allTranslationKeys = new Set();

        // Read the labels of each file
        files.forEach(filePath => {
            let contents = this.fs.readFileSync(filePath, 'utf8');
            contents.split('\n').forEach(line => {
                line = line.trim();
                if (line !== "" && !line.startsWith('#')) {
                    const parts = line.split("=").map(part => part.trim());

                    if (parts.length > 2) {
                        this._addTranslationKey(filePath, parts[0]);
                        allTranslationKeys.add(parts[0]);
                    }
                }
            });
        });

        // Now check which keys are missing for each file
        files.forEach(filePath => {
            let compareSet = new Set(allTranslationKeys);
            let fileSet = this._getTranslationSet(filePath);

            fileSet.forEach(key => compareSet.delete(key));
            this._extendTranslationFile(filePath, compareSet);

            // Add for statistic reasons
            missingTranslationKeysCount += compareSet.size;
        });

        console.log("\n" + missingTranslationKeysCount + " translation keys were added.\n");
    }
}

/**
 * Parse cli arguments
 */
process.argv.forEach(function (val, index, array)
{
    let argumentParts = val.split("=");

    if (argumentParts.length === 2) {
        if (argumentParts[0] === "--sync") {
            const sync = new SyncI18nTranslations(argumentParts[1]);
            sync.sync();
        }
    }
});
